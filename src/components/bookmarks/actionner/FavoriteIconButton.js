import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
// Icons
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
// Custom
import bookmarkActions from '../../../_actions/bookmark.actions';

const styles = theme => ({
  button: {
    padding: theme.spacing.unit,
    color: theme.palette.grey['600']
  },
  wrapper: {
    position: 'relative'
  },
  progress: {
    color: theme.palette.primary,
    position: 'absolute',
    top: 3,
    left: 3,
    zIndex: 1
  },
  flagged: {
    color: theme.palette.action.primary
  }
});

class FavoriteIconButton extends React.Component {
  handleFavoriteItem = () => {
    this.props.favoriteItem(this.props.item.bid, {
      flags: { ...this.props.item.flags, favorite: !this.props.item.flags.favorite }
    });
  };

  render() {
    const { classes, item, favoriteOperationState } = this.props;
    return (
      <div className={classes.wrapper}>
        <IconButton
          title={!item.flags.favorite ? 'Favorite' : 'Unfavorite'}
          className={classNames(classes.button, { [classes.flagged]: item.flags.favorite })}
          aria-label={!item.flags.favorite ? 'Favorite' : 'Unfavorite'}
          onClick={this.handleFavoriteItem}
          disabled={favoriteOperationState[item.bid] && favoriteOperationState[item.bid].loading}
        >
          {!item.flags.favorite ? (
            <StarBorderIcon fontSize="small" />
          ) : (
            <StarIcon fontSize="small" />
          )}
        </IconButton>
        {favoriteOperationState[item.bid] && favoriteOperationState[item.bid].loading && (
          <CircularProgress size={30} className={classes.progress} />
        )}
      </div>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    favoriteItem: (bid, bookmark) => dispatch(bookmarkActions.update(bid, bookmark, 'FAVORITE'))
  };
}

function mapStateToProps(state) {
  const favoriteOperationState = state.bookmarks.operations.FAVORITE;
  return {
    favoriteOperationState
  };
}

const ConnectedFavoriteIconButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(FavoriteIconButton);

export default withStyles(styles)(ConnectedFavoriteIconButton);
