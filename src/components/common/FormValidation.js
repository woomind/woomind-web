import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import FormError from './FormError';

const styles = () => ({
  form: {
    width: '100%' // Fix IE 11 issue.
  }
});

/**
 * Generic FormValidator component.
 * This component defines a <form> HTML element where some of the components must mind an external validation.
 * This is used to enrich the withValidation client side validation of the components with some server error that could
 * be available in the redux state.
 *
 * onSubmit() can optionnaly be passed
 * validation is a mandatory array of the inner fields for which server side validation must be tracked.
 */
class FormValidation extends React.Component {
  // Overrides the constructor to dynamically define the internal state structure.
  constructor(props) {
    super(props);
    this.state = {};
    const { children } = this.props;

    // Create state element for each validable child.
    React.Children.forEach(children, element => {
      if (!React.isValidElement(element)) return;
      const { name } = element.props;

      this.state[name] = {
        ref: React.createRef(),
        isValid: true
      };
    });
  }

  // This callback is passed to withValidation() HOC and is called every time the component validates.
  onValidate = (isValid, componentName) => {
    const newState = this.state;
    newState[componentName].isValid = isValid;
    this.setState(newState);
  };

  // Form submit handler.
  handleSubmit = event => {
    // Prevent page to reload.

    event.preventDefault();
    const { children, validation } = this.props;
    let formIsValid = true;

    // Validate all input fields:
    // this is done by triggering a onBlur() event on all of them.
    // NOTE: switching focus actually blurs it.
    React.Children.forEach(children, element => {
      if (!React.isValidElement(element)) return;

      // The following only applies to fields defined as validable via the "validation" props array passed to the form.
      if (validation.indexOf(element.props.name) >= 0) {
        const { name } = element.props;
        if (this.state[name].ref.current) {
          // withValidation() validates on blur, so we need to force this event to trigger.
          this.state[name].ref.current.focus();
          this.state[name].ref.current.blur();
          formIsValid = formIsValid && this.state[name].isValid;
        }
      }
    });

    // Only if the form is valid from client-side perceptive: we do run the custom form onSubmit().
    if (this.props.onSubmit && formIsValid) {
      this.props.onSubmit(event);
    }
  };

  render() {
    const {
      children,
      classes,
      validation,
      operation,
      formError,
      dispatch,
      onSubmit,
      ...other
    } = this.props;
    return (
      <form onSubmit={this.handleSubmit} className={classes.form} noValidate {...other}>
        {/* Add form error display. */}
        {!!formError && !formError.fields && (
          <FormError type={formError.type} error={formError.message} />
        )}
        {/* Update "validation" child components with necessary data. */}
        {React.Children.map(children, child => {
          if (!React.isValidElement(child)) return child;
          if (validation.indexOf(child.props.name) >= 0) {
            return React.cloneElement(child, {
              onValidate: this.onValidate,
              inputRef: this.state[child.props.name].ref,
              operation
            });
          }
          return child;
        })}
      </form>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    formError: state[ownProps.operation] && state[ownProps.operation].error
  };
}

const connectedFormValidation = connect(mapStateToProps)(FormValidation);
export default withStyles(styles)(connectedFormValidation);
