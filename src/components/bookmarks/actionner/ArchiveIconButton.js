import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
// Icons
import ArchiveOutlinedIcon from '@material-ui/icons/ArchiveOutlined';
import UnArchiveIcon from '@material-ui/icons/Unarchive';
// Custom
import bookmarkActions from '../../../_actions/bookmark.actions';

const styles = theme => ({
  button: {
    padding: theme.spacing.unit,
    color: theme.palette.grey['600']
  },
  wrapper: {
    position: 'relative'
  },
  progress: {
    color: theme.palette.primary,
    position: 'absolute',
    top: 3,
    left: 3,
    zIndex: 1
  },
  flagged: {
    color: theme.palette.action.primary
  }
});

class ArchiveIconButton extends React.Component {
  handleArchiveItem = () => {
    this.props.updateItem(this.props.item.bid, {
      flags: { ...this.props.item.flags, archive: !this.props.item.flags.archive }
    });
  };

  render() {
    const { classes, item, archiveOperationState } = this.props;
    return (
      <div className={classes.wrapper}>
        <IconButton
          title={!item.flags.archive ? 'Archive' : 'Unarchive'}
          className={classNames(classes.button, { [classes.flagged]: item.flags.archive })}
          aria-label={!item.flags.archive ? 'Archive' : 'Unarchive'}
          onClick={this.handleArchiveItem}
          disabled={archiveOperationState[item.bid] && archiveOperationState[item.bid].loading}
        >
          {!item.flags.archive ? (
            <ArchiveOutlinedIcon fontSize="small" />
          ) : (
            <UnArchiveIcon fontSize="small" />
          )}
        </IconButton>
        {archiveOperationState[item.bid] && archiveOperationState[item.bid].loading && (
          <CircularProgress size={30} className={classes.progress} />
        )}
      </div>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    updateItem: (bid, bookmark) => dispatch(bookmarkActions.update(bid, bookmark, 'ARCHIVE'))
  };
}

function mapStateToProps(state) {
  const archiveOperationState = state.bookmarks.operations.ARCHIVE;
  return {
    archiveOperationState
  };
}

const ConnectedArchiveIconButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(ArchiveIconButton);

export default withStyles(styles)(ConnectedArchiveIconButton);
