import React from 'react';
// Material UI
import withStyles from '@material-ui/core/styles/withStyles';
import { connect } from 'react-redux';

/**
 * Defines a HOC function able to add custom generic validation implementation capability to a WrappedComponent passed in
 * parameter.
 *
 * @param WrappedComponent
 * @returns * The WrappedComponent added with this custom implementation.
 */
const withValidation = WrappedComponent => {
  // Theme error displayed below field.
  const styles = theme => ({
    error: {
      fontWeight: 500,
      fontSize: '0.9rem',
      backgroundColor: theme.palette.error.light,
      padding: `0.5rem 1rem`,
      margin: `0 0.5rem 0 0`,
      flex: 1,
      minHeight: '2em'
    }
  });

  /**
   * Defines a wrapper component for the field to be validated.
   */
  class WithValidationComponent extends React.Component {
    state = {
      isValid: true,
      error: '',
      value: this.props.value || '',
      apiCalledAt: 0
    };

    /**
     * The purpose here is to extract the isValid and errorMessage data from what API sends back.
     * API data are sent via redux to redux state, mapped to props via connect() (see below) and finally
     * moved in state here.
     * This is required because server validation should erase if user update the field value: either with client
     * validation error, or with no error at all if the field is now valid.
     * For instance, if test@test.com email is already taken (during registration), we must disable this message once
     * the user has entered another email.
     *
     * @param nextProps
     * @param prevState
     * @returns {*} The newState
     */
    static getDerivedStateFromProps(nextProps, prevState) {
      const nextState = prevState;

      // Note: This (getDerivedStateFromProps) function is called everytime the component renders, when props or state
      // are changed. We want the following to occur only when the props from redux changed (hence the timestamp), and
      // if their are error sent from the API available in redux.
      if (nextProps.apiError && nextProps.timestamp !== prevState.apiCalledAt) {
        nextState.apiCalledAt = nextProps.timestamp;
        nextState.isValid = false;
        nextState.error = nextProps.apiError;
        return nextState;
      }
      return null;
    }

    /**
     * Validates a required value.
     * @param value
     * @returns {{isValid: boolean, error: string, value: *}}
     */
    validateRequired = value => {
      const isValid = value.length;
      return {
        value,
        isValid: !!isValid,
        error: isValid ? '' : `${this.props.label} cannot be empty.`
      };
    };

    /**
     * Validates an Email format.
     * @param value
     * @returns {{isValid: boolean, error: string, value: *}}
     */
    validateEmail = value => {
      const isValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
      return {
        value,
        isValid: !!isValid,
        error: isValid ? '' : `${this.props.label} format is invalid.`
      };
    };

    /**
     * Validates a URL format.
     * @param value
     * @returns {{isValid: boolean, error: string, value: *}}
     */
    validateUrl = value => {
      const isValid = value.match(/^(h|$)(t|$)(t|$)(p|$)(s?|$)(:|$)(\/|$)(\/|$)/);
      return {
        value,
        isValid: !!isValid,
        error: isValid ? '' : `${this.props.label} must start with http:// or https://`
      };
    };

    // This callback is called every time the input value is updated.
    handleInputChange = e => {
      const { value } = e.target;
      this.setState({ value });
    };

    // This callback is called every time the input value is blur (ie when focus is lost).
    handleInputValidate = e => {
      const { value } = e.target;

      // Build new state: the former one with value updated.
      let validation = this.state;
      validation.value = value;

      // Check if field is empty.
      if (this.props.required) {
        validation = this.validateRequired(value);
      }

      // Check if field is of email type.
      if (validation.isValid && this.props.type === 'email') {
        validation = this.validateEmail(value);
      }

      // Check if field is of email type.
      if (validation.isValid && this.props.type === 'url') {
        validation = this.validateUrl(value);
      }

      // If onValidate() callback is defined, use it.
      if (this.props.onValidate) {
        this.props.onValidate(validation.isValid, this.props.id);
      }

      // Set the results (and re-render).
      this.setState(validation);
    };

    render() {
      const {
        value,
        classes,
        error,
        helperText,
        onChange,
        onBlur,
        dispatch,
        apiError,
        onValidate,
        ...other
      } = this.props;
      return (
        <WrappedComponent
          value={this.state.value}
          error={!this.state.isValid || error}
          helperText={(!this.state.isValid && this.state.error) || helperText}
          FormHelperTextProps={{
            classes: {
              error: classes.error
            }
          }}
          onChange={e => {
            this.handleInputChange(e);
            if (onChange) onChange(e);
          }}
          onBlur={e => {
            this.handleInputValidate(e);
            if (onBlur) onBlur(e);
          }}
          {...other}
        />
      );
    }
  }

  /**
   * Map redux-state to props. The "operation" name is propagated to here so we can know where in the structure
   * we can find this particular field error value.
   * This is done by convention using the operation and the field name props.
   * @param state
   * @param ownProps
   * @returns {{apiError: *, timestamp: number}}
   */
  function mapStateToProps(state, ownProps) {
    let apiError;
    if (ownProps.operation) {
      try {
        apiError = state[ownProps.operation].error.fields[ownProps.name];
      } catch (e) {
        /* Do nothing */
      }
    }

    return {
      timestamp: Date.now(), // Mind this timestamp (@see getDerivedStateFromProps)
      apiError
    };
  }

  const connectedWithValidationComponent = connect(mapStateToProps)(WithValidationComponent);
  return withStyles(styles)(connectedWithValidationComponent);
};

export default withValidation;
