import React from 'react';
import { connect } from 'react-redux';
// Material UI
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import LinearProgress from '@material-ui/core/LinearProgress';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
// Imports
import userActions from '../../_actions/user.actions';
import Link from '../common/Link';
import FormValidation from '../common/FormValidation';
import PasswordInputField from '../common/PasswordInputField';
import withValidation from '../hoc/withValidation';

const ValidatedTextField = withValidation(TextField);

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8
  },
  innerContent: {
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative'
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  alerting: {
    marginTop: theme.spacing.unit * 3
  },
  remember: {
    marginTop: theme.spacing.unit * 3
  },
  submit: {
    marginTop: theme.spacing.unit * 9
  },
  register: {
    marginTop: theme.spacing.unit
  },
  error: {
    fontWeight: 500,
    fontSize: '0.9rem',
    backgroundColor: theme.palette.error.light,
    padding: `0.5rem 1rem`,
    margin: 0
  }
});

class LoginPage extends React.Component {
  handleSubmit = event => {
    event.preventDefault();
    const { email, password } = event.target;
    this.props.logUser(email.value, password.value);
  };

  render() {
    const { loading, classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          {loading ? (
            <div>
              <LinearProgress />
            </div>
          ) : (
            <div style={{ height: 4, width: '100%', display: 'block' }} />
          )}
          <div className={classes.innerContent}>
            <div className={classes.header}>
              <Avatar className={classes.avatar}>
                <LockIcon />
              </Avatar>
              <Typography component="h1" variant="h4">
                Sign In
              </Typography>
              <Typography component="h6">Continue to WooMind</Typography>
            </div>
            <FormValidation
              onSubmit={this.handleSubmit}
              operation="authentication"
              validation={['email', 'password']}
            >
              <ValidatedTextField
                required
                fullWidth
                type="email"
                label="Email"
                id="email"
                name="email"
                autoComplete="email"
                margin="normal"
              />
              <PasswordInputField
                required
                fullWidth
                label="Password"
                id="password"
                name="password"
                autoComplete="current-password"
                margin="normal"
                confirmation={false}
                strength={false}
              />
              <Button
                fullWidth
                disabled={loading}
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Sign in
              </Button>
            </FormValidation>
            <Typography align="center" variant="subtitle1">
              <Link margin="normal" to="/register" variant="primary" className={classes.register}>
                Register
              </Link>
            </Typography>
          </div>
        </Paper>
      </main>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    logUser: (email, password) => dispatch(userActions.login(email, password))
  };
}

function mapStateToProps(state) {
  if (state.authentication) {
    return {
      loading: state.authentication.loading
    };
  }
  return {};
}

const connectedLoginPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

export default withStyles(styles)(connectedLoginPage);
