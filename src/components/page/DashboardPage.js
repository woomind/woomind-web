import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import BookmarkReport from '../bookmarks/report/BookmarkReport';

class DashboardPage extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Route
          path="/dashboard"
          component={() => <Redirect to="/dashboard/bookmarks" />}
          exact={true}
        />
        <Route path="/dashboard/bookmarks" component={BookmarkReport} />
      </React.Fragment>
    );
  }
}

export default DashboardPage;
