import React from 'react';
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
// Icons
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
// Custom
import ConfirmDeleteForm from '../form/ConfirmDeleteForm';

const styles = theme => ({
  button: {
    padding: theme.spacing.unit
  },
  wrapper: {
    position: 'relative'
  },
  progress: {
    color: theme.palette.primary,
    position: 'absolute',
    top: 3,
    left: 3,
    zIndex: 1
  }
});

class DeleteIconButton extends React.Component {
  state = {
    confirmDialog: false
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.deleteOperationState[this.props.item.bid] &&
      prevProps.deleteOperationState[this.props.item.bid].loading === true &&
      !this.props.deleteOperationState[this.props.item.bid].loading
    ) {
      this.setState({ confirmDialog: false });
    }
  }

  /** Handle opening for ConfirmDeleteForm */
  handleConfirmDeleteOpen = () => {
    this.setState({ confirmDialog: true });
  };

  handleConfirmDeleteClose = () => {
    this.setState({ confirmDialog: false });
  };

  render() {
    const { classes, item, deleteOperationState } = this.props;
    return (
      <div className={classes.wrapper}>
        <IconButton
          title="Delete"
          aria-label="Delete"
          aria-haspopup="true"
          className={classes.button}
          disableRipple={true}
          onClick={this.handleConfirmDeleteOpen}
          disabled={deleteOperationState[item.bid] && deleteOperationState[item.bid].loading}
        >
          <DeleteIcon fontSize="small" />
        </IconButton>

        {deleteOperationState[item.bid] && deleteOperationState[item.bid].loading && (
          <CircularProgress size={30} className={classes.progress} />
        )}
        <ConfirmDeleteForm
          open={this.state.confirmDialog}
          item={item}
          onClose={this.handleConfirmDeleteClose}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const deleteOperationState = state.bookmarks.operations.DELETE;
  return {
    deleteOperationState
  };
}

const ConnectedDeleteIconButton = connect(mapStateToProps)(DeleteIconButton);

export default withStyles(styles)(ConnectedDeleteIconButton);
