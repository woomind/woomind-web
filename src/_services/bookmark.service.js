import ServiceUnavailableError from '../_helpers/errors';
import getAuthTokenHeader from '../_helpers/authenticationHeader';

/**
 * Handle a fetch() response to API.
 * @param response
 * @returns string|rejectedPromise
 */
function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      return Promise.reject(data.error);
    }
    return data.data;
  });
}

/**
 * Fetch all bookmarks
 *
 * @param user
 */
function getAll(user = null) {
  const requestOptions = {
    method: 'GET',
    headers: getAuthTokenHeader()
  };
  const url = user
    ? `${process.env.API_URL}/bookmarks/user/${user.uid}`
    : `${process.env.API_URL}/bookmarks`;

  return fetch(url, requestOptions)
    .catch(() => Promise.reject(new ServiceUnavailableError()))
    .then(handleResponse)
    .then(bookmarks => bookmarks);
}

/**
 * Delete a bookmark
 *
 * @param bid
 *  * @param user (optional)
 */
function deleteBookmark(bid, user = null) {
  const requestOptions = {
    method: 'DELETE',
    headers: getAuthTokenHeader()
  };
  const url = user
    ? `${process.env.API_URL}/bookmarks/${bid}/user/${user.uid}`
    : `${process.env.API_URL}/bookmarks/${bid}`;

  return fetch(url, requestOptions)
    .catch(() => Promise.reject(new ServiceUnavailableError()))
    .then(handleResponse)
    .then(bookmark => bookmark);
}

/**
 * Patch a bookmark
 *
 * @param bid
 * @param data
 * @param user (optional)
 */
function patch(bid, data, user = null) {
  const headers = { 'Content-Type': 'application/json' };
  const requestOptions = {
    method: 'PATCH',
    headers: { ...headers, ...getAuthTokenHeader() },
    body: JSON.stringify(data)
  };
  const url = user
    ? `${process.env.API_URL}/bookmarks/${bid}/user/${user.uid}`
    : `${process.env.API_URL}/bookmarks/${bid}`;

  return fetch(url, requestOptions)
    .catch(() => Promise.reject(new ServiceUnavailableError()))
    .then(handleResponse)
    .then(bookmark => bookmark);
}

/**
 * Create a bookmark with url.
 *
 * @param url
 * @param user (optional)
 */
function create(url, user = null) {
  const headers = { 'Content-Type': 'application/json' };
  const requestOptions = {
    method: 'POST',
    headers: { ...headers, ...getAuthTokenHeader() },
    body: JSON.stringify({ url })
  };
  const apiUrl = user
    ? `${process.env.API_URL}/bookmarks/user/${user.uid}`
    : `${process.env.API_URL}/bookmarks`;

  return fetch(apiUrl, requestOptions)
    .catch(() => Promise.reject(new ServiceUnavailableError()))
    .then(handleResponse)
    .then(bookmark => bookmark);
}

/**
 * Pre-fetch thumbnails for a given bookmark.
 *
 * @param rid
 * @param bid
 */
async function prefetchThumbnails(rid, bid) {
  const baseUrl = `${process.env.API_URL}/fetch/cover?bid=${bid}&rid=${rid}`;
  // Fetch card thumbnail.
  fetch(`${baseUrl}&width=450&height=150`);
  // Fetch list item thumbnail.
  fetch(`${baseUrl}&width=70&height=70`);
}

const bookmarkService = {
  getAll,
  delete: deleteBookmark,
  patch,
  create,
  prefetchThumbnails
};
export default bookmarkService;
