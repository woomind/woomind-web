import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// Material UI
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
  root: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
    borderRadius: '6px',
    height: '1.4rem',
    fontSize: '0.75rem',
    backgroundColor: theme.palette.action.hover,
    color: theme.palette.grey[700],
    '&:hover': {
      backgroundColor: theme.palette.action.active,
      color: theme.palette.common.white,
      cursor: 'pointer'
    }
  },
  label: {
    padding: `0 ${theme.spacing.unit}px`
  },
  deleteIcon: {
    margin: '0 4px 0 -4px',
    width: '1.2rem'
  }
});

function Tag(props) {
  const { children, classes, className, ...other } = props;
  return (
    <Chip
      className={classNames(classes.root, className)}
      classes={{ label: classes.label, deleteIcon: classes.deleteIcon }}
      {...other}
    >
      {children}
    </Chip>
  );
}

export default withStyles(styles)(Tag);
