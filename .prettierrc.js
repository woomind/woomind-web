module.exports = {
  printWidth: 100, // Screen width.
  singleQuote: true // Prefer single quote in strings.
};
