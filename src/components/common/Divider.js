import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';

const styles = theme => ({
  grow: {
    flexGrow: 1
  },
  vertical: {
    flexGrow: 'unset',
    width: 1,
    backgroundColor: fade(theme.palette.common.black, 0.54),
    margin: theme.spacing.unit
  }
});

function Divider(props) {
  return (
    <span
      className={classNames(
        props.classes.grow,
        { [props.classes.vertical]: props.vertical },
        props.className
      )}
    />
  );
}
export default withStyles(styles)(Divider);
