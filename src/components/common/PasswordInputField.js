import React from 'react';
import classNames from 'classnames';
import stringEntropy from 'fast-password-entropy';
// Material UI
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import LinearProgress from '@material-ui/core/LinearProgress';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import withStyles from '@material-ui/core/styles/withStyles';
import amber from '@material-ui/core/colors/amber';
import green from '@material-ui/core/colors/green';
// Custom
import withValidation from '../hoc/withValidation';

// Extend TextField component with client-side validation.
const ValidatedTextField = withValidation(TextField);

const styles = theme => ({
  password: {
    position: 'relative',
    '&:after': {
      border: 'none'
    }
  },
  passwordStrengthMeter: {
    position: 'absolute',
    bottom: -6,
    height: 6,
    left: 0,
    width: '100%',
    transition: theme.transitions.create(['background-color'], {
      duration: 1000
    }),
    backgroundColor: theme.palette.action.disabledBackground
  },
  passwordStrengthMeterError: {
    marginTop: '6px !important'
  },
  weak: {
    backgroundColor: theme.palette.error.dark
  },
  medium: {
    backgroundColor: amber[700]
  },
  good: {
    backgroundColor: green[600]
  }
});

/**
 * Defines a "password" input component with a confirmation field.
 */
class PasswordInputField extends React.Component {
  // Required minimal entropy can be defined via ENV variables.
  requiredEntropy = process.env.SECURITY_PASSWORD_ENTROPY || 45;

  state = {
    password: {
      isValid: true,
      error: '',
      value: '',
      strength: 0,
      showPassword: false,
      ref:
        (this.props.passwordProps && this.props.passwordProps.inputRef) ||
        this.props.inputRef ||
        React.createRef()
    },
    passwordConfirm: {
      isValid: true,
      error: '',
      value: '',
      showPassword: false,
      ref:
        (this.props.passwordConfirmProps && this.props.passwordConfirmProps.inputRef) ||
        React.createRef()
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextState.password.value !== this.state.password.value ||
      nextState.passwordConfirm.value !== this.state.passwordConfirm.value ||
      nextState.password.showPassword !== this.state.password.showPassword ||
      nextState.passwordConfirm.showPassword !== this.state.passwordConfirm.showPassword
    );
  }

  // Handler for password validation.
  validatePassword = e => {
    const { id, value } = e.target;
    const { strength = true } = this.props;
    const entropy = stringEntropy(value);
    const isValid = strength ? !value.length || entropy >= this.requiredEntropy : true;
    const newState = {
      ...this.state,
      password: {
        ...this.state.password,
        value,
        isValid,
        strength: (entropy * 67) / this.requiredEntropy, // NOTE: let password display "green" when requiredEntropy is reached.
        error: isValid ? '' : 'Please choose a stronger password.'
      }
    };
    this.setState(newState);
    // If onValidate() callback is defined, use it.
    if (this.props.onValidate) {
      this.props.onValidate(isValid, id);
    }
  };

  // Handler for toggling "password" show/hide.
  handleClickShowPassword = async inputName => {
    const newState = {
      ...this.state,
      [inputName]: { ...this.state[inputName], showPassword: !this.state[inputName].showPassword }
    };
    await this.setState(newState);
    // Force revalidate.
    this.state[inputName].ref.current.focus();
    this.state[inputName].ref.current.blur();
  };

  // Handle for confirmation password.
  validateConfirmPassword = e => {
    const { id, value } = e.target;
    const isValid = value.length && this.state.password.value === value;
    const error =
      this.state.password.value === value ? 'Password cannot be empty' : 'Passwords do not match.';
    const newState = {
      ...this.state,
      passwordConfirm: {
        ...this.state.passwordConfirm,
        value,
        isValid,
        error: isValid ? '' : error
      }
    };
    this.setState(newState);
    // If onValidate() callback is defined, use it.
    if (this.props.onValidate) {
      this.props.onValidate(isValid, id);
    }
  };

  // This callback is passed to withValidation() HOC and is called every time the component validates.
  onValidate(isValid, componentName) {
    const newState = this.state;
    newState[componentName].isValid = isValid;
    if (this.props.onValidate) {
      this.props.onValidate(
        newState.password.isValid &&
          newState.passwordConfirm.isValid &&
          newState.password.value === newState.passwordConfirm.value,
        componentName
      );
    }
  }

  render() {
    const {
      classes,
      label,
      name,
      error,
      helperText,
      passwordProps,
      passwordConfirmProps,
      toggler = true,
      strength = true,
      confirmation = true,
      inputRef,
      ...other
    } = this.props;
    const { passwordLabel, passwordClass, passwordRef, ...passwordPropsOther } =
      passwordProps || {};
    const {
      passwordConfirmLabel,
      passwordConfirmClass,
      passwordConfirmRef,
      ...passwordConfirmPropsOther
    } = passwordConfirmProps || {};

    return (
      <React.Fragment>
        <ValidatedTextField
          label={label || passwordLabel || 'Password'}
          id="password"
          name={name || 'password'}
          type={this.state.password.showPassword ? 'text' : 'password'}
          className={classNames(classes.password, passwordClass)}
          classes={
            (strength && {
              error: classes.passwordStrengthMeterError
            }) ||
            {}
          }
          error={!this.state.password.isValid || error}
          helperText={(!this.state.password.isValid && this.state.password.error) || helperText}
          onChange={this.validatePassword}
          onValidate={this.onValidate}
          inputRef={this.state.password.ref}
          InputProps={
            (strength || toggler) && {
              classes: {
                root: classes.password
              },
              endAdornment: (
                <InputAdornment position="end">
                  {toggler && (
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={() => this.handleClickShowPassword('password')}
                    >
                      {this.state.password.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  )}
                  {strength && (
                    <LinearProgress
                      className={classes.passwordStrengthMeter}
                      classes={{
                        bar: classNames({
                          [classes.weak]: this.state.password.strength > 0,
                          [classes.medium]: this.state.password.strength > 33,
                          [classes.good]: this.state.password.strength > 66
                        })
                      }}
                      variant="determinate"
                      value={this.state.password.strength}
                    />
                  )}
                </InputAdornment>
              )
            }
          }
          {...other}
          {...passwordPropsOther}
        />

        {confirmation && (
          <ValidatedTextField
            error={!this.state.passwordConfirm.isValid}
            helperText={!this.state.passwordConfirm.isValid && this.state.passwordConfirm.error}
            label={passwordConfirmLabel || 'Confirm Password'}
            id="passwordConfirm"
            name="passwordConfirm"
            type={this.state.passwordConfirm.showPassword ? 'text' : 'password'}
            className={classNames(classes.passwordConfirm, passwordConfirmClass)}
            onBlur={this.validateConfirmPassword}
            onValidate={this.onValidate}
            inputRef={this.state.passwordConfirm.ref}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={() => this.handleClickShowPassword('passwordConfirm')}
                  >
                    {this.state.passwordConfirm.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              )
            }}
            {...other}
            {...passwordConfirmPropsOther}
          />
        )}
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(PasswordInputField);
