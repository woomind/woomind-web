import React from 'react';
import classNames from 'classnames';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
// Icons
import ViewListIcon from '@material-ui/icons/ViewList';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';

const styles = theme => ({
  button: {
    padding: theme.spacing.unit / 2,
    color: theme.palette.action.disabled
  },
  selected: {
    color: theme.palette.action.active
  }
});

function ListDisplayMenu(props) {
  const { classes, display } = props;
  return (
    <React.Fragment>
      <IconButton
        title="Compact view"
        arial-label="Compact view"
        className={classNames(classes.button, { [classes.selected]: display === 'compact' })}
        size="small"
        aria-haspopup="true"
        onClick={props.onChange('compact')}
      >
        <ViewHeadlineIcon />
      </IconButton>

      <IconButton
        title="List view"
        arial-label="List view"
        className={classNames(classes.button, { [classes.selected]: display === 'list' })}
        size="small"
        aria-haspopup="true"
        onClick={props.onChange('list')}
      >
        <ViewListIcon />
      </IconButton>

      <IconButton
        title="Card view"
        arial-label="Card view"
        className={classNames(classes.button, { [classes.selected]: display === 'card' })}
        size="small"
        aria-haspopup="true"
        onClick={props.onChange('card')}
      >
        <ViewModuleIcon />
      </IconButton>
    </React.Fragment>
  );
}

export default withStyles(styles)(ListDisplayMenu);
