import React from 'react';
import classNames from 'classnames';
import { format, parseISO, formatDistance } from 'date-fns';
// Material UI
import { withStyles } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Zoom from '@material-ui/core/Zoom';
import { fade } from '@material-ui/core/styles/colorManipulator';
// Icons
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
// Custom
import TagsDisplay from '../../common/TagsDisplay';
import EditIconButton from '../actionner/EditIconButton';
import DeleteIconButton from '../actionner/DeleteIconButton';
import FavoriteIconButton from '../actionner/FavoriteIconButton';
import ArchiveIconButton from '../actionner/ArchiveIconButton';
import ReadLaterIconButton from '../actionner/ReadLaterIconButton';

const styles = theme => ({
  actions: {
    alignSelf: 'flex-end',
    width: '95%',
    margin: '0 auto',
    padding: theme.spacing.unit / 2
  },
  content: {
    flex: '1 0 auto',
    wordBreak: 'break-all'
  },
  grow: {
    flexGrow: 1
  },
  divider: {
    margin: `0 ${theme.spacing.unit * 2}px`
  },
  bottomSpace: {
    marginBottom: theme.spacing.unit
  },
  avatarFavicon: {
    border: '1px solid',
    backgroundColor: 'transparent',
    borderColor: theme.palette.type === 'light' ? theme.palette.grey[400] : theme.palette.grey[600]
  },
  favicon: {
    width: 16,
    height: 16,
    verticalAlign: 'middle',
    marginRight: theme.spacing.unit,
    marginBottom: theme.spacing.unit / 4,
    backgroundSize: 'contain',
    display: 'inline-block',
    color: 'initial'
  },
  item: {
    backgroundColor: theme.palette.background.paper,
    width: `calc(16.666% - ${theme.spacing.unit * 2}px)`,
    display: 'inline-flex',
    flexDirection: 'column',
    margin: theme.spacing.unit,
    [theme.breakpoints.down('xl')]: {
      width: `calc(20% - ${theme.spacing.unit * 2}px)`
    },
    [theme.breakpoints.down('lg')]: {
      width: `calc(25% - ${theme.spacing.unit * 2}px)`
    },
    [theme.breakpoints.down('md')]: {
      width: `calc(33.333% - ${theme.spacing.unit * 2}px)`
    },
    [theme.breakpoints.down('sm')]: {
      width: `calc(50% - ${theme.spacing.unit * 2}px)`
    },
    [theme.breakpoints.down('600')]: {
      width: `100%`
    }
  },
  title: {
    fontSize: '1.2rem',
    fontWeight: 500,
    color: fade(theme.palette.common.black, 0.77)
  },
  mediaWrapper: {
    position: 'relative',
    height: 150,
    width: '100%'
  },
  media: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    backgroundPosition: 'top center',
    backgroundSize: 'cover'
  },
  ellipsisText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  link: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    paddingRight: '2rem',
    minWidth: '10rem'
  },
  time: {
    fontSize: '0.8rem',
    lineHeight: '0.8rem',
    marginTop: '0.4rem',
    marginBottom: '0.1rem',
    whiteSpace: 'pre-line',
    textAlign: 'right',
    flex: 1
  },
  tags: {
    display: 'flex',
    margin: `0 ${theme.spacing.unit * 2}px`,
    flexWrap: 'wrap'
  }
});

class BookmarkCardItem extends React.Component {
  render() {
    const { classes, bookmark, index } = this.props;
    const createdAt = parseISO(bookmark.createdAt);
    return (
      <Zoom
        in={!bookmark.removed}
        style={{ transitionDelay: `${index * 50}ms` }}
        timeout={bookmark.removed ? 500 : 225}
        unmountOnExit
      >
        <Card className={classes.item}>
          <CardHeader
            classes={{
              content: classes.ellipsisText
            }}
            titleTypographyProps={{
              className: classNames(classes.ellipsisText, classes.title),
              title: [bookmark.title]
            }}
            title={bookmark.title}
            subheader={
              <React.Fragment>
                <Link
                  rel="noreferrer"
                  target="_blank"
                  href={bookmark.url}
                  title={bookmark.url}
                  color="inherit"
                  className={classes.link}
                >
                  {bookmark.favicon ? (
                    <CardMedia
                      component="span"
                      className={classes.favicon}
                      image={bookmark.favicon}
                    />
                  ) : (
                    <ImportContactsIcon className={classes.favicon} />
                  )}
                  {bookmark.domain}
                </Link>
                <time title={format(createdAt, 'MMM dd, yyyy')} className={classes.time}>
                  {formatDistance(createdAt, new Date(), { addSuffix: true })}
                </time>
              </React.Fragment>
            }
            subheaderTypographyProps={{ style: { display: 'flex' } }}
          />
          <div className={classes.mediaWrapper}>
            <CardMedia className={classes.media} image={'/images/neutral.jpg'} />
            <CardMedia
              className={classes.media}
              image={`${process.env.API_URL}/fetch/cover?bid=${bookmark.bid}&rid=${
                bookmark.rid
              }&width=450&height=150`}
            />
          </div>
          <CardContent className={classes.content}>
            <Typography component="p">{bookmark.description}</Typography>
          </CardContent>

          <TagsDisplay className={classes.tags} component="span" tags={bookmark.tags} />

          <Divider className={classes.divider} />
          <CardActions className={classes.actions} disableActionSpacing>
            <FavoriteIconButton item={bookmark} />

            <ReadLaterIconButton item={bookmark} />

            <ArchiveIconButton item={bookmark} />

            <div className={classes.grow} />

            <EditIconButton item={bookmark} />

            <DeleteIconButton item={bookmark} />
          </CardActions>
        </Card>
      </Zoom>
    );
  }
}
export default withStyles(styles)(BookmarkCardItem);
