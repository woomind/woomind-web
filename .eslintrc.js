module.exports = {
  parser: 'babel-eslint',
  extends: [
    'airbnb-base',
    'prettier',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:react/recommended'
  ],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',

    // Same as AirBnB, but adds `ret` to exclusions
    // rule: https://eslint.org/docs/rules/no-param-reassign.html
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'acc', // for reduce accumulators
          'accumulator', // for reduce accumulators
          'e', // for e.returnvalue
          'ctx', // for Koa routing
          'req', // for Express requests
          'request', // for Express requests
          'res', // for Express responses
          'ret', // for Mongoose expressions.
          'response', // for Express responses
          '$scope' // for Angular 1 scopes
        ]
      }
    ],
    // Same as AirBnB, but adds necessary exclusions.
    // rule: https://eslint.org/docs/rules/no-param-reassign.html
    'no-underscore-dangle': [
      'error',
      {
        allow: ['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__', '__STORE__']
      }
    ],
    'react/prop-types': [0]
  },
  env: {
    browser: true,
    node: true,
    jest: true
  }
};
