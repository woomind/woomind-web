import React from 'react';
import { connect } from 'react-redux';
// Material UI
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import LinearProgress from '@material-ui/core/LinearProgress';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
// Imports
import FormError from '../common/FormError';
import Link from '../common/Link';
import PasswordInputField from '../common/PasswordInputField';
import userActions from '../../_actions/user.actions';
import withValidation from '../hoc/withValidation';

const ValidatedTextField = withValidation(TextField);

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8
  },
  innerContent: {
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative',
    margin: '0 1rem',
    textAlign: 'center'
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%' // Fix IE 11 issue.
  },
  alerting: {
    marginTop: theme.spacing.unit * 3
  },
  subtext: {
    marginTop: '1em'
  },
  remember: {
    marginTop: theme.spacing.unit * 3
  },
  submit: {
    clear: 'both',
    marginTop: '4em'
  },
  signIn: {
    marginTop: theme.spacing.unit
  }
});

/**
 * Special note: because of the PasswordInputField which is pretty special,
 * we can't use the generic <FormValidation> component.
 *
 * Hence, here is mostly some duplicate code form FormValidation component.
 * @TODO maybe we could find a way to handle non standard TextField component in withValidation()
 */
class RegisterPage extends React.Component {
  state = {
    email: {
      ref: React.createRef(),
      isValid: true
    },
    password: {
      ref: React.createRef(),
      isValid: true
    },
    passwordConfirm: {
      ref: React.createRef(),
      isValid: true
    }
  };

  // This callback is passed to withValidation() HOC and is called every time the component validates.
  onValidate = (isValid, componentName) => {
    const newState = this.state;
    newState[componentName].isValid = isValid;
    this.setState(newState);
  };

  // Form submit handler.
  handleSubmit = event => {
    // Prevent page to reload.
    event.preventDefault();
    const { email, password } = event.target;

    // Validate all input fields:
    // this is done by triggering a onBlur() event on all of them.
    // NOTE: switching focus actually blurs it.
    this.state.email.ref.current.focus();
    this.state.password.ref.current.focus();
    this.state.passwordConfirm.ref.current.focus();
    this.state.passwordConfirm.ref.current.blur();

    if (
      this.state.email.isValid &&
      this.state.password.isValid &&
      this.state.passwordConfirm.isValid
    ) {
      const newState = this.state;
      this.setState(newState);
      this.props.registerUser(email.value, password.value);
    }
  };

  render() {
    const { loading, apiError, classes } = this.props;

    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          {loading ? (
            <div>
              <LinearProgress />
            </div>
          ) : (
            <div style={{ height: 4, width: '100%', display: 'block' }} />
          )}
          <div className={classes.innerContent}>
            <div className={classes.header}>
              <Avatar className={classes.avatar}>
                <LockIcon />
              </Avatar>
              <Typography component="h1" variant="h4">
                Welcome to Woomind
              </Typography>
            </div>

            <form onSubmit={this.handleSubmit} className={classes.form} noValidate>
              {!!apiError && !apiError.fields && (
                <FormError type={apiError.type} error={apiError.message} />
              )}
              <ValidatedTextField
                required
                fullWidth
                label="Email"
                id="email"
                name="email"
                type="email"
                margin="dense"
                autoComplete="email"
                operation="registration"
                onValidate={this.onValidate}
                inputRef={this.state.email.ref}
              />

              <PasswordInputField
                required
                fullWidth
                name="password"
                margin="dense"
                operation="registration"
                autoComplete="new-password"
                passwordProps={{
                  inputRef: this.state.password.ref
                }}
                passwordConfirmProps={{
                  inputRef: this.state.passwordConfirm.ref
                }}
                onValidate={this.onValidate}
              />

              <Button
                fullWidth
                disabled={loading}
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Create an account
              </Button>
              <Typography align="center" variant="subtitle1">
                <Link margin="normal" to="/login" variant="primary" className={classes.signIn}>
                  Log in instead
                </Link>
              </Typography>
            </form>
          </div>
        </Paper>
      </main>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    registerUser: (email, password) => dispatch(userActions.register({ email, password }))
  };
}

function mapStateToProps(state) {
  if (state.registration) {
    return {
      loading: state.registration.loading,
      apiError: state.registration.error
    };
  }
  return {};
}

const connectedRegisterPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterPage);

export default withStyles(styles)(connectedRegisterPage);
