import notifyActions from './notify.actions';
import userService from '../_services/user.service';
import userConstants from '../_constants/user.constants';
import history from '../_helpers/history';

function login(email, password) {
  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
  return dispatch => {
    dispatch(request({ email }));

    userService.login(email, password).then(
      user => {
        dispatch(success(user));
        dispatch(notifyActions.success(`Welcome back ${user.email}`));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };
}

function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}

function register(user) {
  function request(data) {
    return { type: userConstants.REGISTER_REQUEST, data };
  }

  function success(data) {
    return { type: userConstants.REGISTER_SUCCESS, data };
  }

  function failure(error) {
    return { type: userConstants.REGISTER_FAILURE, error };
  }
  return dispatch => {
    dispatch(request(user));

    userService.register(user).then(
      data => {
        dispatch(success(data));
        history.push('/login');
        dispatch(notifyActions.success('Registration successful'));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };
}

// NOTE: delete is a reserved keyword.
// We will map it map to delete in default export below.
function deleteUser(uid) {
  function request(id) {
    return { type: userConstants.DELETE_REQUEST, id };
  }
  function success(id) {
    return { type: userConstants.DELETE_SUCCESS, id };
  }
  function failure(id, error) {
    return { type: userConstants.DELETE_FAILURE, id, error };
  }
  return dispatch => {
    dispatch(request(uid));

    userService
      .delete()
      .then(user => dispatch(success(user.uid)), error => dispatch(failure(uid, error.toString())));
  };
}

export default {
  login,
  logout,
  register,
  delete: deleteUser
};
