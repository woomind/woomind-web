import userConstants from '../_constants/user.constants';
import storageService from '../_services/storage.service';

// Get user from storage.
const user = storageService.get('user');

const initialState = user ? { loggedIn: true, user } : {};
export default (state = initialState, action) => {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loading: true
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user,
        token: action.token
      };
    case userConstants.LOGIN_FAILURE:
      return { error: action.error };
    case userConstants.LOGOUT:
      return {};
    default:
      return state;
  }
};
