import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// Material UI
import Tag from './Tag';

const styles = theme => ({
  label: {
    padding: `0 ${theme.spacing.unit}px`
  }
});

class TagsDisplay extends React.Component {
  render() {
    const { classes, className, tags = {}, component: Component = 'div', ...other } = this.props;
    return (
      <Component className={classNames(classes.root, className)} {...other}>
        {tags.map(tag => (
          <Tag component={Component} key={tag} label={tag} />
        ))}
      </Component>
    );
  }
}

export default withStyles(styles)(TagsDisplay);
