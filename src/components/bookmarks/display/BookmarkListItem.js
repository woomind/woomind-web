import React from 'react';
import classNames from 'classnames';
import { format, parseISO, formatDistance } from 'date-fns';
// Material UI
import { withStyles } from '@material-ui/core';
import CardMedia from '@material-ui/core/CardMedia';
import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import { fade } from '@material-ui/core/styles/colorManipulator';
// Icons
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
// Custom
import TagsDisplay from '../../common/TagsDisplay';
import EditIconButton from '../actionner/EditIconButton';
import DeleteIconButton from '../actionner/DeleteIconButton';
import FavoriteIconButton from '../actionner/FavoriteIconButton';
import ArchiveIconButton from '../actionner/ArchiveIconButton';
import ReadLaterIconButton from '../actionner/ReadLaterIconButton';

const styles = theme => ({
  grow: {
    display: 'block',
    content: ' ',
    width: 2,
    margin: `0 ${theme.spacing.unit}px`,
    height: theme.spacing.unit * 4,
    backgroundColor: theme.palette.divider
  },
  avatar: {
    width: 50,
    height: 50,
    margin: 10
  },
  avatarWithIcon: {
    border: '1px solid',
    backgroundColor: 'transparent',
    borderColor: theme.palette.type === 'light' ? theme.palette.grey[400] : theme.palette.grey[600]
  },
  content: {
    flex: 1
  },
  title: {
    fontSize: '1.1rem',
    fontWeight: 500,
    color: fade(theme.palette.common.black, 0.77),
    flex: '1 0 100%'
  },
  mediaWrapper: {
    position: 'relative',
    height: 70,
    width: 70,
    [theme.breakpoints.down('500')]: {
      display: 'none'
    }
  },
  media: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    backgroundPosition: 'top center'
  },
  item: {
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    padding: `0 ${theme.spacing.unit}px 0 0`,
    '&:hover': {
      backgroundColor: theme.palette.action.hover
    }
  },
  favicon: {
    width: 16,
    height: 16,
    verticalAlign: 'middle',
    marginRight: theme.spacing.unit,
    marginBottom: theme.spacing.unit / 4,
    backgroundSize: 'contain',
    display: 'inline-block',
    color: 'initial'
  },
  ellipsisText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  divider: {
    height: theme.spacing.unit
  },
  secondary: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    paddingRight: '10rem',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
    }
  },
  link: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    flex: 1,
    [theme.breakpoints.down('sm')]: {
      flex: '1 0 70%'
    },
    [theme.breakpoints.down('500')]: {
      flex: '1 0 100%'
    }
  },
  time: {
    fontSize: '0.8rem',
    textAlign: 'right',
    flex: 1,
    position: 'absolute',
    right: 0,
    top: '50%',
    lineHeight: '0.8rem',
    marginTop: '-0.4rem',
    [theme.breakpoints.down('sm')]: {
      paddingRight: 0,
      position: 'initial',
      flex: '1 0 30%',
      textAlign: 'right',
      margin: '0.2rem 0 0 0'
    },
    [theme.breakpoints.down('500')]: {
      display: 'none'
    }
  },
  tag: {
    flex: 1
  },
  actions: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  }
});

class BookmarkListItem extends React.Component {
  state = {
    menuShow: false
  };

  handleMenuShow = () => {
    this.setState({ menuShow: true });
  };

  handleMenuHide = () => {
    this.setState({ menuShow: false });
  };

  render() {
    const { classes, bookmark, index } = this.props;
    const createdAt = parseISO(bookmark.createdAt);
    return (
      <Slide
        in={!bookmark.removed}
        direction="up"
        style={{ transitionDelay: `${index * 50}ms` }}
        timeout={bookmark.removed ? 500 : 225}
        unmountOnExit
      >
        <React.Fragment>
          <ListItem
            className={classes.item}
            onMouseEnter={this.handleMenuShow}
            onMouseLeave={this.handleMenuHide}
          >
            <div className={classes.mediaWrapper}>
              <CardMedia className={classes.media} image={'/images/neutral.jpg'} />
              <CardMedia
                className={classes.media}
                image={`${process.env.API_URL}/fetch/cover?bid=${bookmark.bid}&rid=${
                  bookmark.rid
                }&width=70&height=70`}
              />
            </div>

            <ListItemText
              className={classes.content}
              secondary={
                <React.Fragment>
                  <Typography
                    variant="body1"
                    component="div"
                    className={classNames(classes.ellipsisText, classes.title)}
                    title={bookmark.title}
                  >
                    {bookmark.title}
                  </Typography>
                  <Link
                    rel="noreferrer"
                    className={classes.link}
                    target="_blank"
                    href={bookmark.url}
                    title={bookmark.url}
                    color="inherit"
                  >
                    {bookmark.favicon ? (
                      <CardMedia
                        component="span"
                        className={classes.favicon}
                        image={bookmark.favicon}
                      />
                    ) : (
                      <ImportContactsIcon className={classes.favicon} />
                    )}
                    {bookmark.domain}
                  </Link>
                  <time title={format(createdAt, 'MMM dd, yyyy')} className={classes.time}>
                    {formatDistance(createdAt, new Date(), { addSuffix: true })}
                  </time>
                  <TagsDisplay className={classes.tags} component="span" tags={bookmark.tags} />
                </React.Fragment>
              }
              secondaryTypographyProps={{
                component: 'div',
                className: classes.secondary
              }}
            />
            <div className={classes.actions}>
              <FavoriteIconButton item={bookmark} />
              <ReadLaterIconButton item={bookmark} />
              <ArchiveIconButton item={bookmark} />
              <EditIconButton item={bookmark} />
              <DeleteIconButton item={bookmark} />
            </div>
          </ListItem>
          <Divider />
        </React.Fragment>
      </Slide>
    );
  }
}
export default withStyles(styles)(BookmarkListItem);
