import React from 'react';
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
// Icons
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
// Custom
import EditBookmarkForm from '../form/EditBookmarkForm';

const styles = theme => ({
  button: {
    padding: theme.spacing.unit
  },
  wrapper: {
    position: 'relative'
  },
  progress: {
    color: theme.palette.primary,
    position: 'absolute',
    top: 3,
    left: 3,
    zIndex: 1
  }
});

class EditIconButton extends React.Component {
  state = {
    editDialog: false
  };

  componentDidUpdate(prevProps) {
    if (prevProps.editOperationState.loading === true && !this.props.editOperationState.loading) {
      this.setState({ editDialog: false });
    }
  }

  /** Handle opening for EditForm */
  handleEditOpen = () => {
    this.setState({ editDialog: true });
  };

  handleEditClose = () => {
    this.setState({ editDialog: false });
  };

  render() {
    const { classes, item, editOperationState } = this.props;
    return (
      <div className={classes.wrapper}>
        <IconButton
          title="Edit"
          aria-label="Edit"
          className={classes.button}
          disableRipple={true}
          onClick={this.handleEditOpen}
          disabled={editOperationState.loading}
        >
          <EditIcon fontSize="small" />
        </IconButton>
        {editOperationState.loading && <CircularProgress size={30} className={classes.progress} />}
        <EditBookmarkForm open={this.state.editDialog} item={item} onClose={this.handleEditClose} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const editOperationState = state.bookmarks.operations.UPDATE;
  return {
    editOperationState
  };
}

const ConnectedEditIconButton = connect(mapStateToProps)(EditIconButton);

export default withStyles(styles)(ConnectedEditIconButton);
