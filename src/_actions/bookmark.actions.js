import notifyActions from './notify.actions';
import userActions from './user.actions';
import bookmarkService from '../_services/bookmark.service';
import bookmarkConstants from '../_constants/bookmark.constants';

function getAll(user = null) {
  function request(requestUser = null) {
    return { type: bookmarkConstants.GET_ALL.REQUEST, requestUser };
  }
  function success(bookmarks) {
    return { type: bookmarkConstants.GET_ALL.SUCCESS, bookmarks };
  }
  function failure(error) {
    return { type: bookmarkConstants.GET_ALL.FAILURE, error };
  }
  return dispatch => {
    dispatch(request(user));

    bookmarkService.getAll(user).then(
      bookmarks => {
        dispatch(success(bookmarks));
      },
      error => {
        if (error.status === 401 || error.status === 403) {
          dispatch(userActions.logout());
        }
        dispatch(failure(error));
        dispatch(notifyActions.warning(error.message));
      }
    );
  };
}

function deleteBookmark(bid, operation = 'DELETE') {
  function request() {
    return { type: bookmarkConstants.DELETE.REQUEST, operation, bid };
  }
  function success(bookmark) {
    return { type: bookmarkConstants.DELETE.SUCCESS, operation, bookmark };
  }
  function failure(error) {
    return { type: bookmarkConstants.DELETE.FAILURE, bid, operation, error };
  }
  return dispatch => {
    dispatch(request());

    bookmarkService.delete(bid).then(
      bookmark => {
        dispatch(success(bookmark));
      },
      error => {
        if (error.status === 401 || error.status === 403) {
          dispatch(userActions.logout());
        }
        dispatch(failure(error));
        dispatch(notifyActions.warning(error.message));
      }
    );
  };
}

function update(bid, data, operation = 'UPDATE') {
  function request() {
    return {
      type: bookmarkConstants[operation].REQUEST,
      operation,
      bid,
      data
    };
  }
  function success(bookmark) {
    return { type: bookmarkConstants[operation].SUCCESS, operation, bookmark };
  }
  function failure(error) {
    return { type: bookmarkConstants[operation].FAILURE, operation, bid, error };
  }
  return dispatch => {
    dispatch(request());

    bookmarkService.patch(bid, data).then(
      updatedBookmark => {
        dispatch(success(updatedBookmark));
      },
      error => {
        if (error.status === 401 || error.status === 403) {
          dispatch(userActions.logout());
        }
        dispatch(failure(error));
        dispatch(notifyActions.warning(error.message));
      }
    );
  };
}

function create(url, operation = 'CREATE') {
  function request() {
    return {
      type: bookmarkConstants[operation].REQUEST,
      operation,
      url
    };
  }
  function success(bookmark) {
    return { type: bookmarkConstants[operation].SUCCESS, operation, bookmark };
  }
  function failure(error) {
    return { type: bookmarkConstants[operation].FAILURE, operation, url, error };
  }
  return dispatch => {
    dispatch(request());

    bookmarkService.create(url).then(
      bookmark => {
        // Prefetch all thumbnails format generation.
        bookmarkService.prefetchThumbnails(bookmark.rid, bookmark.bid);
        dispatch(success(bookmark));
      },
      error => {
        if (error.status === 401 || error.status === 403) {
          dispatch(userActions.logout());
        }
        dispatch(failure(error));
        dispatch(notifyActions.warning(error.message));
      }
    );
  };
}

export default {
  getAll,
  delete: deleteBookmark,
  update,
  create
};
