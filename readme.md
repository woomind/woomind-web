# Welcome to the WooMind WEB client !

### Clone the repository

```
$ git clone https://github.com/dclause/woomind-web
```

### Checkout the `develop` branch

```
$ git checkout develop
```

### Build the project locally

```
$ npm install
```

### Run Commands

```npm run build:prod``` - Builds the project as of production ready  
```npm run build:dev``` - Builds the project, ready for debug  
```npm start``` -  Starts the project (once built)  
```npm run dev-server``` - Run a development self reloading server using Webpack-dev-server  
```npm test``` - Run tests over the project  
