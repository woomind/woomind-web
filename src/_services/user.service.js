import authHeader from '../_helpers/authenticationHeader';
import ServiceUnavailableError from '../_helpers/errors';
import storageService from './storage.service';

/**
 * Handle a fetch() response to API.
 * @param response
 * @returns string|rejectedPromise
 */
function handleResponse(response) {
  const authToken = response.headers.get('X-Auth-Token');
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      return Promise.reject(data.error);
    }
    if (authToken) {
      data.data.token = authToken;
    }
    return data.data;
  });
}

/**
 * Logout a user.
 */
function logout() {
  // Remove user from storage to log user out.
  storageService.delete('user');
}

/**
 * Log a user in.
 *
 * @param email
 * @param password
 * @returns {Promise<Response | never>}
 */
function login(email, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email, password })
  };

  return fetch(`${process.env.API_URL}/users/authenticate`, requestOptions)
    .catch(() => Promise.reject(new ServiceUnavailableError()))
    .then(handleResponse)
    .then(user => {
      // Store user details and jwt token in local storage to keep user logged in between page refreshes.
      storageService.set('user', JSON.stringify(user));
      return user;
    });
}

/**
 * Get user full profile info.
 *
 * @returns {Promise<Response | never>}
 */
function getUser() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${process.env.API_URL}/user`, requestOptions).then(handleResponse);
}

/**
 * Registers a user.
 *
 * @param user
 * @returns {Promise<Response | never>}
 */
function register(user) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  };

  return fetch(`${process.env.API_URL}/users/register`, requestOptions)
    .catch(() => Promise.reject(new ServiceUnavailableError()))
    .then(handleResponse)
    .then(newUser => newUser);
}

/**
 * Update a user profile info.
 *
 * @param user
 * @returns {Promise<Response | never>}
 */
function update(user) {
  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  };

  return fetch(`${process.env.API_URL}/user`, requestOptions).then(handleResponse);
}

/**
 * Deletes a user.
 *
 * NOTE: Because 'delete' is a JS reserved keyword, we temporarily use another name for this method.
 * @param id
 * @returns {Promise<Response | never>}
 */
function deleteUser() {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader()
  };

  return fetch(`${process.env.API_URL}/user`, requestOptions).then(handleResponse);
}

const userService = {
  login,
  logout,
  register,
  getUser,
  update,
  delete: deleteUser
};
export default userService;
