import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

// import AccountCircle from '@material-ui/icons/AccountCircle';
import AppBar from '@material-ui/core/AppBar';
// import Badge from '@material-ui/core/Badge';
import CssBaseline from '@material-ui/core/CssBaseline';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import IconButton from '@material-ui/core/IconButton';
// import SettingsIcon from '@material-ui/icons/Settings';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreIcon from '@material-ui/icons/MoreVert';
// import NotificationsIcon from '@material-ui/icons/Notifications';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';

import SearchBar from '../common/SearchBar';
import LogoutConfirmForm from '../common/LogoutConfirmForm';
import userActions from '../../_actions/user.actions';

const drawerWidth = 240;

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    marginLeft: theme.spacing.unit * 7,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  searchRoot: {
    margin: `0 ${theme.spacing.unit * 7}px`,
    boxShadow: 'none',
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    height: 36,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    }
  },
  searchButton: {
    opacity: 1,
    padding: `0 ${theme.spacing.unit}px`
  },
  searchIconSearch: {
    opacity: 1,
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    height: 36,
    color: 'inherit',
    width: '100%',
    transition: theme.transitions.create('width')
  },
  inputInput: {
    color: theme.palette.common.white,
    padding: theme.spacing.unit,
    transition: theme.transitions.create('width'),
    width: '100%'
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  }
});

class Header extends React.Component {
  state = {
    openLogoutConfirm: false,
    anchorEl: null,
    mobileMoreAnchorEl: null
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  handleLogoutConfirmOpen = () => {
    this.setState({ openLogoutConfirm: true });
  };

  handleLogoutConfirmClose = () => {
    this.setState({ openLogoutConfirm: false });
  };

  render() {
    // { anchorEl, mobileMoreAnchorEl, openLogoutConfirm } = this.state;
    const { mobileMoreAnchorEl, openLogoutConfirm } = this.state;
    const { classes } = this.props;
    // const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}
      >
        {/* <MenuItem>
          <IconButton color="inherit">
            <SettingsIcon />
          </IconButton>
          <p>Preferences</p>
        </MenuItem>
        <MenuItem>
          <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <p>Notifications</p>
        </MenuItem>
        <MenuItem onClick={this.handleProfileMenuOpen}>
          <IconButton color="inherit">
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem> */}
        <MenuItem onClick={this.handleLogoutConfirmOpen}>
          <IconButton color="inherit">
            <ExitToAppIcon />
          </IconButton>
          <p>Logout</p>
        </MenuItem>
      </Menu>
    );

    return (
      <AppBar
        position="absolute"
        className={classNames(classes.appBar, this.props.open && classes.appBarShift)}
      >
        <CssBaseline />
        <Toolbar>
          <Typography className={classes.title} variant="h4" component="h1" color="inherit">
            WooMind
          </Typography>
          <div className={classes.grow} />
          <SearchBar
            classes={{
              root: classes.searchRoot,
              iconButton: classes.searchButton,
              searchContainer: classes.inputRoot,
              input: classes.inputInput,
              searchIcon: classes.searchIcon
            }}
            placeholder="Filters..."
            value={this.state.search}
            onChange={newValue => this.setState({ search: newValue })}
            onRequestSearch={() => console.log(this.state.search)}
          />

          <div className={classes.sectionDesktop}>
            {/* <IconButton color="inherit" title="Preferences" aria-label="Preferences">
              <SettingsIcon />
            </IconButton>

            <IconButton color="inherit" title="Notifications" aria-label="Notifications">
              <Badge badgeContent={4} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>

            <IconButton
              title="Profile"
              aria-label="Profile"
              aria-owns={isMenuOpen ? 'material-appbar' : undefined}
              aria-haspopup="true"
              onClick={this.handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton> */}
            <IconButton
              title="Logout"
              arial-label="Logout"
              onClick={this.handleLogoutConfirmOpen}
              color="inherit"
            >
              <ExitToAppIcon />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
        {renderMobileMenu}
        <LogoutConfirmForm open={openLogoutConfirm} onClose={this.handleLogoutConfirmClose} />
      </AppBar>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  LogoutConfirmForm: () => dispatch(userActions.LogoutConfirmForm())
});
const ConnectedHeader = connect(
  undefined,
  mapDispatchToProps
)(Header);

export default withStyles(styles)(ConnectedHeader);
