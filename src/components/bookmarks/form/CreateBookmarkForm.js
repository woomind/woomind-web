import React from 'react';
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import LinearProgress from '@material-ui/core/LinearProgress';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';
// Custom
import bookmarkActions from '../../../_actions/bookmark.actions';
import FormValidation from '../../common/FormValidation';
import withValidation from '../../hoc/withValidation';

const ValidatedTextField = withValidation(TextField);

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const styles = theme => ({
  cancel: {
    color: theme.palette.text.secondary
  }
});

class CreateBookmarkForm extends React.Component {
  handleCreateItem = event => {
    event.preventDefault();
    const { url } = event.target;
    this.props.createItem(url.value);
  };

  render() {
    const { classes, fullScreen, createOperationState } = this.props;

    return (
      <Dialog
        fullWidth
        fullScreen={fullScreen}
        open={this.props.open}
        TransitionComponent={Transition}
        onClose={this.props.onClose}
        aria-labelledby="create-form-title"
      >
        {createOperationState.loading && <LinearProgress />}
        <DialogTitle id="create-form-title">{'Add new bookmark'}</DialogTitle>
        <DialogContent>
          <FormValidation onSubmit={this.handleCreateItem} validation={['url']}>
            <ValidatedTextField
              required
              fullWidth
              type="url"
              pattern="^https?\:\/\/"
              placeholder="https://..."
              autoFocus={true}
              label="Bookmark URL"
              id="url"
              name="url"
              margin="dense"
            />
            <DialogActions>
              <Button type="submit" disabled={createOperationState.loading} color="primary">
                Create
              </Button>
              <Button
                disabled={createOperationState.loading}
                onClick={this.props.onClose}
                className={classes.cancel}
              >
                Cancel
              </Button>
            </DialogActions>
          </FormValidation>
        </DialogContent>
      </Dialog>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    createItem: url => dispatch(bookmarkActions.create(url))
  };
}

function mapStateToProps(state) {
  const createOperationState = state.bookmarks.operations.CREATE;
  return {
    createOperationState
  };
}

const ConnectedCreateBookmarkForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateBookmarkForm);

const StyledConnectedCreateBookmarkForm = withStyles(styles)(ConnectedCreateBookmarkForm);
export default withMobileDialog()(StyledConnectedCreateBookmarkForm);
