import notifyConstants from '../_constants/notify.constants';

const defaultState = {
  notifications: []
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case notifyConstants.ENQUEUE:
      return {
        ...state,
        notifications: [
          ...state.notifications,
          {
            ...action.notification
          }
        ]
      };

    case notifyConstants.DEQUEUE:
      return {
        ...state,
        notifications: state.notifications.filter(notification => notification.key !== action.key)
      };

    default:
      return state;
  }
};
