import React from 'react';
// Material UI
import { withStyles } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
// Icons
import AddIcon from '@material-ui/icons/Add';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import ArchiveIcon from '@material-ui/icons/Archive';
import ChatIcon from '@material-ui/icons/Chat';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import MusicNoteIcon from '@material-ui/icons/MusicNote';
import PanoramaIcon from '@material-ui/icons/Panorama';
import RssFeedIcon from '@material-ui/icons/RssFeed';
import StarIcon from '@material-ui/icons/Star';
import SubscriptionsIcon from '@material-ui/icons/Subscriptions';
import WatchLaterIcon from '@material-ui/icons/WatchLater';
// Custom

const styles = theme => ({
  subHeader: {
    textAlign: 'center'
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4
  }
});

export class PredefinedFilterMenu extends React.Component {
  state = {
    open: false
  };

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <ListItem button>
          <ListItemIcon>
            <ImportContactsIcon />
          </ListItemIcon>
          <ListItemText primary="Articles" />
        </ListItem>

        {/* <ListItem button onClick={this.handleClick}>
          <ListItemIcon>
            <AddIcon />
          </ListItemIcon>
          <ListItemText inset primary="More filters..." />
          {this.state.open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
        </ListItem>

        <Collapse in={this.state.open && this.props.open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding className={classes.nested}>
            <span>
              <ListItem button disabled>
                <ListItemIcon>
                  <PanoramaIcon />
                </ListItemIcon>
                <ListItemText primary="Images" />
              </ListItem>
            </span>

            <span>
              <ListItem button disabled>
                <ListItemIcon>
                  <SubscriptionsIcon />
                </ListItemIcon>
                <ListItemText primary="Videos" />
              </ListItem>
            </span>

            <span>
              <ListItem button disabled>
                <ListItemIcon>
                  <MusicNoteIcon />
                </ListItemIcon>
                <ListItemText primary="Music" />
              </ListItem>
            </span>

            <span>
              <ListItem button disabled>
                <ListItemIcon>
                  <AttachFileIcon />
                </ListItemIcon>
                <ListItemText primary="Documents" />
              </ListItem>
            </span>

            <span>
              <ListItem button disabled>
                <ListItemIcon>
                  <ChatIcon />
                </ListItemIcon>
                <ListItemText primary="Notes" />
              </ListItem>
            </span>

            <span>
              <ListItem button disabled>
                <ListItemIcon>
                  <RssFeedIcon />
                </ListItemIcon>
                <ListItemText primary="RSS Feeds" />
              </ListItem>
            </span>
          </List>
        </Collapse> */}

        <Divider />
        <ListItem button>
          <ListItemIcon>
            <StarIcon />
          </ListItemIcon>
          <ListItemText primary="Favorited" />
        </ListItem>

        <ListItem button>
          <ListItemIcon>
            <WatchLaterIcon />
          </ListItemIcon>
          <ListItemText primary="Read later" />
        </ListItem>

        <ListItem button>
          <ListItemIcon>
            <ArchiveIcon />
          </ListItemIcon>
          <ListItemText primary="Archived" />
        </ListItem>
      </React.Fragment>
    );
  }
}
export const BookmarkPredefinedFilterMenu = withStyles(styles)(PredefinedFilterMenu);

class CustomFilterMenu extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        {/*<ListSubheader className={classes.subHeader}>Custom filters</ListSubheader>*/}
      </React.Fragment>
    );
  }
}
export const BookmarkCustomFilterMenu = withStyles(styles)(CustomFilterMenu);
