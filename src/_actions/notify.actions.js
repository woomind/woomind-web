import notifyConstants from '../_constants/notify.constants';

export const enqueueSnackbar = notification => ({
  type: notifyConstants.ENQUEUE,
  notification: {
    key: new Date().getTime() + Math.random(),
    ...notification
  }
});

function notify(message, variant) {
  return {
    type: notifyConstants.ENQUEUE,
    notification: {
      key: new Date().getTime() + Math.random(),
      message,
      options: {
        variant
      }
    }
  };
}

function info(message) {
  return {
    type: notifyConstants.ENQUEUE,
    notification: {
      key: new Date().getTime() + Math.random(),
      message,
      options: {
        variant: 'info'
      }
    }
  };
}

function success(message) {
  return {
    type: notifyConstants.ENQUEUE,
    notification: {
      key: new Date().getTime() + Math.random(),
      message,
      options: {
        variant: 'success'
      }
    }
  };
}

function warning(message) {
  return {
    type: notifyConstants.ENQUEUE,
    notification: {
      key: new Date().getTime() + Math.random(),
      message,
      options: {
        variant: 'warning'
      }
    }
  };
}

function error(message) {
  return {
    type: notifyConstants.ENQUEUE,
    notification: {
      key: new Date().getTime() + Math.random(),
      message,
      options: {
        variant: 'error'
      }
    }
  };
}

export const removeSnackbar = key => ({
  type: notifyConstants.DEQUEUE,
  key
});

export default {
  enqueueSnackbar,
  removeSnackbar,
  notify,
  info,
  success,
  warning,
  error
};
