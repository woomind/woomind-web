import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    color: theme.palette.error.dark,
    backgroundColor: theme.palette.error.light,
    width: '100%',
    padding: theme.spacing.unit,
    margin: '1em 0 0.5em 0',
    fontSize: '0.9rem'
  }
});

function FormError(props) {
  const { classes, type, error } = props;
  let errorTitle = 'Service unavailable';
  if (type === 'AUTHENTICATION_ERROR') errorTitle = 'Authentication failed';

  return (
    <Typography component="h6" align="center" className={classes.root}>
      <div style={{ fontWeight: 500 }}>{errorTitle}</div>
      {error}
    </Typography>
  );
}

export default withStyles(styles)(FormError);
