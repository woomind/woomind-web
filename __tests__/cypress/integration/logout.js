describe('Application logout', () => {
  beforeEach(() => {
    cy.login();
  });

  it('Ensures users log out correctly', () => {
    cy.visit('/dashboard/bookmarks');

    cy.get('button[title="Logout"').click();
    cy.get('button[type="button"]')
      .contains('Logout')
      .click();
    cy.url().should('include', '/login');
  });
});
