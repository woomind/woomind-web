import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
// Material UI
import { withStyles } from '@material-ui/core';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
// Custom
import Sidebar from '../../layout/Sidebar';
import Divider from '../../common/Divider';
import bookmarkActions from '../../../_actions/bookmark.actions';
import ListDisplayMenu from '../../menu/ListDisplayMenu';
import BookmarkCardItem from '../display/BookmarkCardItem';
import BookmarkListItem from '../display/BookmarkListItem';
import BookmarkCompactItem from '../display/BookmarkCompactItem';
import CreateIconButton from '../actionner/CreateIconButton';
import bookmarkConstants from '../../../_constants/bookmark.constants';
import storageService from '../../../_services/storage.service';

const styles = theme => ({
  toolbar: {
    display: 'flex',
    padding: `0 ${theme.spacing.unit * 3}px`,
    paddingTop: theme.spacing.unit * 3
  },
  content: {
    marginLeft: theme.spacing.unit * 7,
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden'
  },
  listWrapper: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowY: 'auto',
    padding: theme.spacing.unit * 3,
    paddingTop: 0,
    flex: 1
  },
  listEmpty: {
    marginTop: theme.spacing.unit * -7.5,
    top: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  cardDisplay: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  actions: {
    display: 'flex',
    height: '2rem'
  }
});

class BookmarkReport extends React.Component {
  state = {
    search: '',
    display: storageService.get('display') || 'card'
  };

  /** Change display mode */
  handleDisplayMode = mode => () => {
    this.props.filter();
    this.setState({
      ...this.state,
      display: mode
    });
    storageService.set('display', mode);
  };

  componentDidMount() {
    this.props.fetchData();
  }

  render() {
    const { classes, bookmarks } = this.props;
    const isEmpty = !bookmarks.length;

    return (
      <React.Fragment>
        <Sidebar />
        <div className={classes.content}>
          <div className={classes.toolbar}>
            <Typography variant="h4" component="h2">
              Bookmarks
            </Typography>
            <Divider />
            <div className={classes.actions}>
              <CreateIconButton />
              <Divider vertical />
              <ListDisplayMenu display={this.state.display} onChange={this.handleDisplayMode} />
            </div>
          </div>

          <div className={classNames(classes.listWrapper, { [classes.listEmpty]: isEmpty })}>
            <List className={classes[`${this.state.display}Display`]}>
              {bookmarks.map((bookmark, index) => {
                switch (this.state.display) {
                  case 'compact':
                    return (
                      <BookmarkCompactItem key={bookmark.bid} index={index} bookmark={bookmark} />
                    );
                  case 'list':
                    return (
                      <BookmarkListItem key={bookmark.bid} index={index} bookmark={bookmark} />
                    );
                  case 'card':
                    return (
                      <BookmarkCardItem key={bookmark.bid} index={index} bookmark={bookmark} />
                    );
                  default:
                    return null;
                }
              })}
            </List>

            {isEmpty && (
              <Typography variant="body1">
                Your dashboard is empty. Click on the + button above to start saving webpages.
              </Typography>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    fetchData: () => dispatch(bookmarkActions.getAll()),
    filter: () =>
      dispatch({
        type: bookmarkConstants.POST_DELETE_PURGE
      })
  };
}

function mapStateToProps(state) {
  const { bookmarks } = state.bookmarks;
  return {
    bookmarks
  };
}

const connectedBookmarkReport = connect(
  mapStateToProps,
  mapDispatchToProps
)(BookmarkReport);

export default withStyles(styles)(connectedBookmarkReport);
