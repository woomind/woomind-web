import React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    color: 'inherit',
    textDecoration: 'inherit',
    display: 'inline-block',
    fontWeight: '500',
    '&:hover': {
      textDecoration: 'none'
    }
  },
  primary: {
    color: theme.palette.primary.main
  }
});

function MyLink(props) {
  const { to, children, classes, className, variant, ...other } = props;
  return (
    <Link
      to={to}
      className={classNames(
        classes.root,
        {
          [classes.primary]: variant === 'primary'
        },
        className
      )}
      {...other}
    >
      {children}
    </Link>
  );
}

export default withStyles(styles)(MyLink);
