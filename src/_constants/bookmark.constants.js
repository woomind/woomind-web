const bookmarkConstants = {
  GET_ALL: {
    REQUEST: 'BOOKMARKS_GET_ALL_REQUEST',
    SUCCESS: 'BOOKMARKS_GET_ALL_SUCCESS',
    FAILURE: 'BOOKMARKS_GET_ALL_FAILURE'
  },
  DELETE: {
    REQUEST: 'BOOKMARKS_DELETE_REQUEST',
    SUCCESS: 'BOOKMARKS_DELETE_SUCCESS',
    FAILURE: 'BOOKMARKS_DELETE_FAILURE'
  },
  FAVORITE: {
    REQUEST: 'BOOKMARKS_FAVORITE_REQUEST',
    SUCCESS: 'BOOKMARKS_FAVORITE_SUCCESS',
    FAILURE: 'BOOKMARKS_FAVORITE_FAILURE'
  },
  ARCHIVE: {
    REQUEST: 'BOOKMARKS_ARCHIVE_REQUEST',
    SUCCESS: 'BOOKMARKS_ARCHIVE_SUCCESS',
    FAILURE: 'BOOKMARKS_ARCHIVE_FAILURE'
  },
  READ_LATER: {
    REQUEST: 'BOOKMARKS_READ_LATER_REQUEST',
    SUCCESS: 'BOOKMARKS_READ_LATER_SUCCESS',
    FAILURE: 'BOOKMARKS_READ_LATER_FAILURE'
  },
  CREATE: {
    REQUEST: 'BOOKMARKS_CREATE_REQUEST',
    SUCCESS: 'BOOKMARKS_CREATE_SUCCESS',
    FAILURE: 'BOOKMARKS_CREATE_FAILURE'
  },
  UPDATE: {
    REQUEST: 'BOOKMARKS_UPDATE_REQUEST',
    SUCCESS: 'BOOKMARKS_UPDATE_SUCCESS',
    FAILURE: 'BOOKMARKS_UPDATE_FAILURE'
  },
  POST_DELETE_PURGE: 'BOOKMARKS_POST_DELETE_PURGE'
};
export default bookmarkConstants;
