import React from 'react';
import { connect } from 'react-redux';
// Material UI
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
// Custom imports.
import userActions from '../../_actions/user.actions';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class LogoutConfirmForm extends React.Component {
  handleLogout = () => {
    this.props.logout();
  };

  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onClose}
        TransitionComponent={Transition}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{'Are you sure to logout ?'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            We will be please to see you soon back on our Woomind !
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleLogout} color="primary" autoFocus>
            Logout
          </Button>
          <Button onClick={this.props.onClose} color="secondary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(userActions.logout())
});
const ConnectedLogout = connect(
  null,
  mapDispatchToProps
)(LogoutConfirmForm);

export default ConnectedLogout;
