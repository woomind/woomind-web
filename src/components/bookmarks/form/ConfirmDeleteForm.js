import React from 'react';
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import LinearProgress from '@material-ui/core/LinearProgress';
import Slide from '@material-ui/core/Slide';
// Custom
import bookmarkActions from '../../../_actions/bookmark.actions';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const styles = theme => ({
  cancel: {
    color: theme.palette.text.secondary
  }
});

class ConfirmDeleteForm extends React.Component {
  handleDeleteItem = e => {
    e.preventDefault();
    // Create bookmark.
    this.props.deleteItem(this.props.item.bid);
  };

  render() {
    const { fullScreen, classes, deleteOperationState } = this.props;
    return (
      <Dialog
        fullScreen={fullScreen}
        open={this.props.open}
        TransitionComponent={Transition}
        onClose={this.props.onClose}
        aria-labelledby="confirm-delete-title"
        aria-describedby="confirm-delete-description"
      >
        <form onSubmit={this.handleDeleteItem}>
          {deleteOperationState[this.props.item.bid] &&
            deleteOperationState[this.props.item.bid].loading && <LinearProgress />}

          <DialogTitle id="confirm-delete-title">
            {'Are you sure you want to delete this item ?'}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="confirm-delete-description">
              <em>
                <strong>{`${this.props.item.title}`}</strong>
              </em>{' '}
              will be permanently deleted.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus type="submit" color="primary">
              Delete
            </Button>
            <Button onClick={this.props.onClose} className={classes.cancel}>
              Cancel
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    deleteItem: bid => dispatch(bookmarkActions.delete(bid))
  };
}

function mapStateToProps(state) {
  const deleteOperationState = state.bookmarks.operations.DELETE;
  return {
    deleteOperationState
  };
}

const ConnectedConfirmDeleteForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmDeleteForm);

const StyledConnectedConfirmDeleteForm = withStyles(styles)(ConnectedConfirmDeleteForm);
export default withMobileDialog()(StyledConnectedConfirmDeleteForm);
