/**
 * Sets an item in storage.
 */
function set(key, data) {
  return localStorage.setItem(key, data);
}

/**
 * Gets an item from storage.
 */
function get(key) {
  let value = localStorage.getItem(key);
  try {
    value = JSON.parse(value);
  } catch (e) {
    /* */
  }
  return value;
}

/**
 * Deletes an item from storage.
 */
function deleteFromStorage(key) {
  return localStorage.removeItem(key);
}

const storageService = {
  set,
  get,
  delete: deleteFromStorage
};
export default storageService;
