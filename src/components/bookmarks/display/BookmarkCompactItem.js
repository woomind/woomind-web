import React from 'react';
import classNames from 'classnames';
import { format, parseISO, formatDistance } from 'date-fns';
// Material UI
import { withStyles } from '@material-ui/core';
import CardMedia from '@material-ui/core/CardMedia';
import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';
import { fade } from '@material-ui/core/styles/colorManipulator';
// Icons
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
// Custom
import EditIconButton from '../actionner/EditIconButton';
import DeleteIconButton from '../actionner/DeleteIconButton';
import FavoriteIconButton from '../actionner/FavoriteIconButton';
import ArchiveIconButton from '../actionner/ArchiveIconButton';
import ReadLaterIconButton from '../actionner/ReadLaterIconButton';

const styles = theme => ({
  grow: {
    flex: 1
  },
  first: {
    width: '70%'
  },
  second: {
    width: '30%'
  },
  content: {
    flex: 1
  },
  item: {
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    paddingTop: theme.spacing.unit / 2,
    paddingBottom: theme.spacing.unit / 2,
    '&:hover': {
      backgroundColor: theme.palette.action.hover
    }
  },
  favicon: {
    width: 20,
    height: 20,
    verticalAlign: 'middle',
    marginRight: theme.spacing.unit,
    backgroundSize: 'contain',
    display: 'inline-block',
    color: 'initial',
    [theme.breakpoints.down('500')]: {
      display: 'none'
    }
  },
  ellipsisText: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },
  secondary: {
    display: 'flex'
  },
  title: {
    flex: '1 0 60%',
    fontSize: '1.1rem',
    fontWeight: 500,
    color: fade(theme.palette.common.black, 0.77),
    alignSelf: 'center'
  },
  link: {
    flex: '1 0 15%',
    margin: '0 5%',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    alignSelf: 'center'
  },
  time: {
    flex: '1 0 10%',
    fontSize: '0.8rem',
    lineHeight: '0.8rem',
    textAlign: 'right',
    alignSelf: 'center',
    [theme.breakpoints.down('500')]: {
      display: 'none'
    }
  },
  actions: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  }
});

class BookmarkCompactItem extends React.Component {
  state = {
    menuShow: false
  };

  handleMenuShow = () => {
    this.setState({ menuShow: true });
  };

  handleMenuHide = () => {
    this.setState({ menuShow: false });
  };

  render() {
    const { classes, bookmark, index } = this.props;
    const createdAt = parseISO(bookmark.createdAt);
    return (
      <Slide
        in={!bookmark.removed}
        direction="up"
        style={{ transitionDelay: `${index * 50}ms` }}
        timeout={bookmark.removed ? 500 : 225}
        unmountOnExit
      >
        <React.Fragment>
          <ListItem
            className={classes.item}
            onMouseEnter={this.handleMenuShow}
            onMouseLeave={this.handleMenuHide}
          >
            {bookmark.favicon ? (
              <CardMedia component="span" className={classes.favicon} image={bookmark.favicon} />
            ) : (
              <ImportContactsIcon className={classes.favicon} />
            )}
            <ListItemText
              secondary={
                <React.Fragment>
                  <Typography
                    variant="body1"
                    component="div"
                    className={classNames(classes.ellipsisText, classes.title)}
                    title={bookmark.title}
                  >
                    {bookmark.title}
                  </Typography>
                  <div className={classes.link}>
                    <Link
                      rel="noreferrer"
                      target="_blank"
                      href={bookmark.url}
                      title={bookmark.url}
                      color="inherit"
                    >
                      {bookmark.domain}
                    </Link>
                  </div>
                  <time title={format(createdAt, 'MMM dd, yyyy')} className={classes.time}>
                    {formatDistance(createdAt, new Date(), { addSuffix: true })}
                  </time>
                </React.Fragment>
              }
              secondaryTypographyProps={{
                component: 'div',
                className: classes.secondary
              }}
              className={classes.second}
            />
            <div className={classes.actions}>
              <FavoriteIconButton item={bookmark} />
              <ReadLaterIconButton item={bookmark} />
              <ArchiveIconButton item={bookmark} />
              <EditIconButton item={bookmark} />
              <DeleteIconButton item={bookmark} />
            </div>
          </ListItem>
          <Divider />
        </React.Fragment>
      </Slide>
    );
  }
}
export default withStyles(styles)(BookmarkCompactItem);
