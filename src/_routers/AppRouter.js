import React from 'react';
import { Redirect, Route, Router, Switch } from 'react-router-dom';
import history from '../_helpers/history';
import DashboardPage from '../components/page/DashboardPage';
import NotFoundPage from '../components/page/NotFoundPage';
import LoginPage from '../components/page/LoginPage';
import RegisterPage from '../components/page/RegisterPage';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import Notifier from '../components/common/Notifier';

const AppRouter = () => (
  <React.Fragment>
    <Notifier />
    <Router history={history}>
      <Switch>
        <PrivateRoute path="/dashboard" component={DashboardPage} />
        <PublicRoute path="/login" component={LoginPage} exact={true} />
        <PublicRoute path="/register" component={RegisterPage} exact={true} />
        <PublicRoute path="/" component={() => <Redirect to="/dashboard" />} exact={true} />
        <Route component={NotFoundPage} />
      </Switch>
    </Router>
  </React.Fragment>
);

export default AppRouter;
