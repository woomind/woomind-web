import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import { SnackbarProvider } from 'notistack';
import AppRouter from './_routers/AppRouter';

import configureStore from './_helpers/store';
import './styles/styles.scss';
import * as serviceWorker from './serviceWorker';

const store = configureStore();

const theme = createMuiTheme({
  palette: {
    primary: blue,
    error: {
      light: '#ffe1d6',
      main: '#f44336',
      dark: '#d32f2f'
    }
  },
  typography: {
    useNextVariants: true
  }
});

const App = () => (
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <SnackbarProvider maxSnack={3}>
        <AppRouter />
      </SnackbarProvider>
    </Provider>
  </MuiThemeProvider>
);

ReactDOM.render(<App />, document.getElementById('app'));

// Opt-in to Progressive WebApp for your app to work offline and load faster
// Change unregister() to register() to opt-in.
serviceWorker.unregister();
