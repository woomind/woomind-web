describe('Authentication form', () => {
  it('Ensures the authentication fails when all fields are empty', () => {
    cy.visit('/login');

    cy.get('form').within(() => {
      cy.contains('Sign in').click();
      cy.contains('Email cannot be empty.');
      cy.contains('Password cannot be empty.');
    });
  });

  it('Ensures the email field works as expected', () => {
    cy.visit('/login');

    cy.get('form').within(() => {
      // Confirm entering an invalid email fails validation.
      cy.get('#email')
        .type('test@test')
        .should('have.value', 'test@test');
      cy.contains('Sign in').click();
      cy.contains('Email format is invalid.');

      // Confirm entering a valid email passes validation.
      cy.get('input[id="email"]')
        .clear()
        .type('test@test.com')
        .should('have.value', 'test@test.com');
      cy.contains('Sign in').click();
      cy.contains('Email format is invalid.').should('not.exist');
    });
  });

  it('Ensures the password field works as expected', () => {
    cy.visit('/login');

    cy.get('form').within(() => {
      // Confirm entering an incorrect password fails validation.
      cy.get('input[id="password"]')
        .type('1234')
        .should('have.value', '1234');
      cy.contains('Sign in').click();

      // Clicking on the password reveal button should work as expected.
      cy.get('[type="password"]');
      cy.get('button:first').click();
      cy.get('[type="text"]');

      // Confirm entering a strong password passes validation.
      cy.get('input[id="password"]')
        .clear()
        .type('Pa$$w0rd!')
        .should('have.value', 'Pa$$w0rd!');
      cy.get('[aria-describedby="password-helper-text"]').should('not.exist');
      cy.contains('Sign in').click();
      cy.contains('Please choose a stronger password.').should('not.exist');
    });
  });

  it('Ensures the app is not vulnerable to Username Enumeration Prevention', () => {
    cy.visit('/login');

    cy.get('form').within(() => {
      cy.get('#email').type('test@test.com');
      cy.get('#password').type('1234{enter}');
      cy.contains('Authentication failed');
      cy.contains('Sign in').click();
    });
  });
});
