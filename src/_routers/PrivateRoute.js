import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Page from '../components/layout/Page';

const PrivateRoute = ({
  isAuthenticated,
  layout: Layout = Page,
  component: Component,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      if (isAuthenticated) {
        return Layout ? <Layout component={Component} /> : <Component {...props} />;
      }
      return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />;
    }}
  />
);

const mapStateToProps = state => ({
  isAuthenticated: state.authentication.loggedIn
});

export default connect(mapStateToProps)(PrivateRoute);
