import bookmarkConstants from '../_constants/bookmark.constants';

export default (
  state = {
    bookmarks: [],
    operations: {
      all: {},
      DELETE: [],
      FAVORITE: [],
      READ_LATER: [],
      ARCHIVE: [],
      CREATE: {},
      UPDATE: {}
    }
  },
  action
) => {
  let { bookmarks } = state;
  const { operations } = state;

  switch (action.type) {
    case bookmarkConstants.GET_ALL.REQUEST:
      return {
        bookmarks,
        operations: {
          ...operations,
          all: {
            loading: true
          }
        }
      };
    case bookmarkConstants.GET_ALL.SUCCESS:
      return {
        bookmarks: action.bookmarks,
        operations: {
          ...operations,
          all: {}
        }
      };
    case bookmarkConstants.GET_ALL.FAILURE:
      delete operations.getAll;
      return {
        bookmarks,
        operations: {
          ...operations,
          all: {}
        }
      };
    case bookmarkConstants.DELETE.REQUEST:
      return {
        bookmarks,
        operations: {
          ...operations,
          DELETE: {
            ...operations.DELETE,
            [action.bid]: {
              loading: true
            }
          }
        }
      };
    case bookmarkConstants.DELETE.SUCCESS:
      // Remove deleted bookmark.
      bookmarks = bookmarks.map(item => {
        if (item.bid === action.bookmark.bid) {
          return { ...item, removed: true };
        }
        return item;
      });
      // Remove delete operation.
      delete operations.DELETE[action.bookmark.bid];
      return {
        bookmarks,
        operations
      };

    case bookmarkConstants.FAVORITE.REQUEST:
    case bookmarkConstants.ARCHIVE.REQUEST:
    case bookmarkConstants.READ_LATER.REQUEST:
      return {
        bookmarks,
        operations: {
          ...operations,
          [[action.operation]]: {
            ...operations[action.operation],
            [action.bid]: {
              loading: true
            }
          }
        }
      };
    case bookmarkConstants.FAVORITE.SUCCESS:
    case bookmarkConstants.ARCHIVE.SUCCESS:
    case bookmarkConstants.READ_LATER.SUCCESS:
      // Update bookmark.
      bookmarks = bookmarks.map(item =>
        item.bid !== action.bookmark.bid ? item : action.bookmark
      );
      // Remove from operation.
      delete operations[action.operation][action.bookmark.bid];
      return {
        bookmarks,
        operations
      };
    case bookmarkConstants.DELETE.FAILURE:
    case bookmarkConstants.FAVORITE.FAILURE:
    case bookmarkConstants.ARCHIVE.FAILURE:
    case bookmarkConstants.READ_LATER.FAILURE:
      // Remove from operation.
      delete operations[action.operation][action.bid];
      return {
        bookmarks,
        operations
      };

    case bookmarkConstants.CREATE.REQUEST:
    case bookmarkConstants.UPDATE.REQUEST:
      return {
        bookmarks,
        operations: {
          ...operations,
          [action.operation]: {
            loading: true,
            url: action.url
          }
        }
      };
    case bookmarkConstants.CREATE.SUCCESS:
      return {
        bookmarks: [action.bookmark, ...bookmarks],
        operations: {
          ...operations,
          [action.operation]: {
            bookmark: action.bookmark
          }
        }
      };
    case bookmarkConstants.UPDATE.SUCCESS:
      // Update bookmark.
      bookmarks = bookmarks.map(item =>
        item.bid !== action.bookmark.bid ? item : action.bookmark
      );
      return {
        bookmarks,
        operations: {
          ...operations,
          [action.operation]: {
            bookmark: action.bookmark
          }
        }
      };
    case bookmarkConstants.CREATE.FAILURE:
    case bookmarkConstants.UPDATE.FAILURE:
      return {
        bookmarks,
        operations: {
          ...operations,
          [action.operation]: {
            error: action.error
          }
        }
      };

    case bookmarkConstants.POST_DELETE_PURGE:
      bookmarks = bookmarks.filter(item => !item.removed);
      return {
        bookmarks,
        operations
      };
    default:
      return state;
  }
};
