const notifyConstants = {
  ENQUEUE: 'ENQUEUE_SNACKBAR',
  DEQUEUE: 'REMOVE_SNACKBAR'
};
export default notifyConstants;
