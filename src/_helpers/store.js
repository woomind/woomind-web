import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import authentication from '../_reducers/auth.reducer';
import registration from '../_reducers/registration.reducer';
import notifications from '../_reducers/notification.reducer';
import bookmarks from '../_reducers/bookmark.reducer';

const loggerMiddleware = createLogger();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () =>
  createStore(
    combineReducers({
      authentication,
      registration,
      notifications,
      bookmarks
    }),
    composeEnhancers(applyMiddleware(thunkMiddleware, loggerMiddleware))
  );
