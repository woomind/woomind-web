describe('Registration form', () => {
  it('Ensures the registration fails when all fields are empty', () => {
    cy.visit('/register');

    cy.get('form').within(() => {
      // We cannot create an account when all fields are empty.
      cy.get('button[type="submit"]')
        .contains('Create an account')
        .click();
      cy.contains('Email cannot be empty.');
      cy.contains('Password cannot be empty.');
      cy.contains('Confirm Password cannot be empty.');
    });
  });

  it('Ensures the email field works as expected', () => {
    cy.visit('/register');

    cy.get('form').within(() => {
      // Entering an invalid email fails validation.
      cy.get('input[id="email"]')
        .type('test@test{enter}')
        .should('have.value', 'test@test');
      cy.contains('Email format is invalid.');

      // Entering a valid email passes validation.
      cy.get('input[id="email"]')
        .clear()
        .type('test@test.com{enter}')
        .should('have.value', 'test@test.com');
      cy.contains('Email format is invalid.').should('not.exist');
    });
  });

  it('Ensures the password field works as expected', () => {
    cy.visit('/register');

    cy.get('form').within(() => {
      // Clicking on the password reveal buttons should work as expected.
      cy.get('[type="password"]');
      cy.get('button[type="button"]').click({ multiple: true });
      cy.get('[type="text"]');

      // To prevent form validation errors from kicking in, we have to enter a
      // valid email address.
      cy.get('input[id="email"]')
        .clear()
        .type('test@test.com{enter}');
      // Confirm entering a weak password fails validation.
      cy.get('input[id="password"]')
        .type('1234{enter}')
        .should('have.value', '1234');
      cy.contains('Please choose a stronger password.');
      cy.get('[aria-describedby="password-helper-text"]');

      // Confirm entering a strong password passes validation.
      cy.get('#password')
        .clear()
        .type('Pa$$w0rd!{enter}')
        .should('have.value', 'Pa$$w0rd!');
      cy.get('[aria-describedby="password-helper-text"]').should('not.exist');
      cy.contains('Please choose a stronger password.').should('not.exist');
    });
  });

  it('Ensures the password confirmation field works as expected', () => {
    cy.visit('/register');

    cy.get('form').within(() => {
      // Confirm entering a different password fails validation.
      cy.get('input:last').should('have.attr', 'id', 'passwordConfirm');
      cy.get('#passwordConfirm')
        .type('1234{enter}')
        .should('have.value', '1234');
      cy.contains('Passwords do not match.');

      // Confirm entering the same password passes validation.
      cy.get('#passwordConfirm')
        .clear()
        .type('Pa$$w0rd!{enter}')
        .should('have.value', 'Pa$$w0rd!');
    });
  });

  it("Ensures registering with the same email address doesn't work", () => {
    cy.visit('/register');

    // Enter already existing email with a strong password.
    cy.get('#email').type('test@test.com');
    cy.get('#password').type('Pa$$w0rd!');
    cy.get('#passwordConfirm').type('Pa$$w0rd!');
    cy.get('button[type="submit"').click();
    cy.contains('Email is already used. Please try another.').should('not.exist');
  });
});
