class ServiceUnavailableError extends Error {
  constructor() {
    super();
    this.message = 'Please try again later.';
    this.type = 'SERVICE_UNAVAILABLE';
  }
}
export default ServiceUnavailableError;
