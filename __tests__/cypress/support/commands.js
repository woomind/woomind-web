// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('login', () => {
  cy.visit('/login');
  cy.get('#email').type('test@test.com');
  cy.get('#password').type('Pa$$w0rd!{enter}');
  // Add delay to prevent subsequent commands from executing before the user is
  // fully logged in.
  cy.wait(500);
});

Cypress.Commands.add('check_favicon', () => {
  // Ensure favicon is displayed correctly.
  cy.get('span').should(
    'have.attr',
    'style',
    'background-image: url("https://www.w3.org/2008/site/images/favicon.ico");'
  );
});

Cypress.Commands.add('check_bookmark_consistency', () => {
  cy.contains('World Wide Web Consortium (W3C)');
  cy.contains('w3.org');
  // To not run into the time ago indication changing while running tests
  // we're being very relaxed here.
  cy.contains('ago');
  cy.get('button[title="Edit"]');
  cy.get('button[title="Delete"]');
});

// Ensure action buttons are working correctly.
Cypress.Commands.add('check_action_buttons', () => {
  cy.get('button[title="Favorite"]').then(buttonsUnchecked => {
    if (buttonsUnchecked) {
      cy.get('button[title="Favorite"]').click();
      cy.get('button[title="Read Later"]').click();
      cy.get('button[title="Archive"]').click();
    }
    cy.get('button[title="Unfavorite"]').then(buttonsChecked => {
      if (buttonsChecked) {
        cy.get('button[title="Unfavorite"]').click();
        cy.get('button[title="Mark as read"]').click();
        cy.get('button[title="Unarchive"]').click();
      }
    });
  });
});
