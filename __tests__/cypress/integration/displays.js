describe('Dashboard behavior and display consistency', () => {
  beforeEach(() => {
    cy.login();
  });

  it('Ensures Compact view works as intended', () => {
    cy.visit('/dashboard/bookmarks');

    cy.get('button[title="Compact view"').click();
    cy.get('li').within(() => {
      cy.check_favicon();
      cy.check_bookmark_consistency();
      cy.check_action_buttons();
    });
  });

  it('Ensures List view works as intended', () => {
    cy.visit('/dashboard/bookmarks');

    cy.get('button[title="List view"').click();
    cy.get('li').within(() => {
      cy.check_favicon();
      cy.check_bookmark_consistency();
      cy.check_action_buttons();
    });
  });

  it('Ensures Card view works as intended', () => {
    cy.visit('/dashboard/bookmarks');

    cy.get('button[title="Card view"').click();
    cy.get('ul').within(() => {
      cy.check_bookmark_consistency();
      // Card view has an additional description field so we check it exists.
      cy.contains('The World Wide Web Consortium (W3C)');
      cy.check_action_buttons();
    });
    cy.get('ul')
      .find('a')
      .within(() => {
        cy.check_favicon();
      });
  });
});
