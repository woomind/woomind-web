describe('CRUD operations on bookmarks', () => {
  beforeEach(() => {
    cy.login();
  });

  it('Ensures an empty dashboard returns the default text', () => {
    cy.visit('/dashboard/bookmarks');

    cy.contains('Your dashboard is empty');
  });

  it('Tests bookmark form validation', () => {
    cy.visit('/dashboard/bookmarks');

    // We cannot pass form validation when no URL was passed.
    cy.get('[title="Add new bookmark"]').click();
    cy.get('input[id="url"]').should('have.attr', 'placeholder', 'https://...');
    cy.get('#create-form-title').click(); // Workaround because the submit button is disabled.
    cy.contains('Bookmark URL cannot be empty.');
    // We cannot pass form validation when the URL is malformed.
    cy.get('input[id="url"]').type('ttps://www.w3.org/');
    cy.get('#create-form-title').click(); // Workaround because the submit button is disabled.
    cy.contains('Bookmark URL must start with http:// or https://');
  });

  it('Ensures adding bookmarks works as expected', () => {
    cy.visit('/dashboard/bookmarks');

    // Add a bookmark by clicking on the Create button.
    cy.get('[title="Add new bookmark"]').click();
    cy.get('input[id="url"]')
      .clear()
      .type('https://www.w3.org/?utm_source=value1&utm_medium=value2');
    cy.get('button[type="submit"')
      .contains('Create')
      .click();

    // Add a bookmark by pressing enter.
    cy.get('[title="Add new bookmark"]').click();
    cy.get('input[id="url"]').type('https://archive.org/{enter}');
  });

  it('Ensures editing bookmarks works as expected', () => {
    cy.visit('/dashboard/bookmarks');

    // Confirm UTM parameter is missing from the URL.
    cy.get('button[title="Edit"]:last').click();
    cy.get('form').within(() => {
      cy.get('input[value="https://www.w3.org/"]');
      cy.get('button[type="submit"]').click();
    });

    // Click on the first bookmark's Edit button.
    cy.get('button[title="Edit"]:first').click();
    cy.get('form').within(() => {
      // Ensures inline form validation works well.
      cy.get('input[id="title"]').clear();
      cy.get('button[type="submit"]').click();
      cy.contains('Title cannot be empty.');
      cy.get('input[id="url"]').clear();
      cy.get('button[type="submit"]').click();
      cy.contains('Bookmark URL cannot be empty.');

      // Change form data.
      cy.get('input[id="title"]')
        .clear()
        .type('Archive (edited)')
        .should('have.value', 'Archive (edited)');
      cy.get('input[id="url"]')
        .clear()
        .type('https://archive.org/?edited')
        .should('have.value', 'https://archive.org/?edited');
      cy.get('textarea[id="description"]')
        .clear()
        .type('Archive description (edited)')
        .should('have.value', 'Archive description (edited)');
      cy.get('input[id="tags"]').type('knowledge internet history ');
    });

    // Ensure checkboxes work correctly.
    cy.get('[type="checkbox"]').check({ force: true }); // Elements aren't visible so we force them to be checked.
    cy.get('button[type="submit"').click();
  });

  it('Ensures deleting bookmarks works as expected', () => {
    cy.visit('/dashboard/bookmarks');

    // Click on the first bookmark's Delete button.
    cy.get('button[title="Delete"]:first').click();
    cy.get('button[type="submit"').click();
  });
});
