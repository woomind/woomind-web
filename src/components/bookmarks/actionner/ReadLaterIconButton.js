import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
// Icons
import WatchLaterIcon from '@material-ui/icons/WatchLater';
import WatchLaterOutlinedIcon from '@material-ui/icons/WatchLaterOutlined';
// Custom
import bookmarkActions from '../../../_actions/bookmark.actions';

const styles = theme => ({
  button: {
    padding: theme.spacing.unit,
    color: theme.palette.grey['600']
  },
  wrapper: {
    position: 'relative'
  },
  progress: {
    color: theme.palette.primary,
    position: 'absolute',
    top: 3,
    left: 3,
    zIndex: 1
  },
  flagged: {
    color: theme.palette.action.primary
  }
});

class ReadLaterIconButton extends React.Component {
  handleReadLaterItem = () => {
    this.props.readLaterItem(this.props.item.bid, {
      flags: { ...this.props.item.flags, read_later: !this.props.item.flags.read_later }
    });
  };

  render() {
    const { classes, item, readLaterOperationState } = this.props;
    return (
      <div className={classes.wrapper}>
        <IconButton
          title={!item.flags.read_later ? 'Read Later' : 'Mark as read'}
          className={classNames(classes.button, { [classes.flagged]: item.flags.read_later })}
          aria-label={!item.flags.read_later ? 'Read Later' : 'Mark as read'}
          onClick={this.handleReadLaterItem}
          disabled={readLaterOperationState[item.bid] && readLaterOperationState[item.bid].loading}
        >
          {!item.flags.read_later ? (
            <WatchLaterOutlinedIcon fontSize="small" />
          ) : (
            <WatchLaterIcon fontSize="small" />
          )}
        </IconButton>
        {readLaterOperationState[item.bid] && readLaterOperationState[item.bid].loading && (
          <CircularProgress size={30} className={classes.progress} />
        )}
      </div>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    readLaterItem: (bid, bookmark) => dispatch(bookmarkActions.update(bid, bookmark, 'READ_LATER'))
  };
}

function mapStateToProps(state) {
  const readLaterOperationState = state.bookmarks.operations.READ_LATER;
  return {
    readLaterOperationState
  };
}

const ConnectedReadLaterIconButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(ReadLaterIconButton);

export default withStyles(styles)(ConnectedReadLaterIconButton);
