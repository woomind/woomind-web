import React from 'react';
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import blue from '@material-ui/core/colors/blue';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import LinearProgress from '@material-ui/core/LinearProgress';
import Slide from '@material-ui/core/Slide';
import TextField from '@material-ui/core/TextField';

// Icons
import ArchiveOutlinedIcon from '@material-ui/icons/ArchiveOutlined';
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import UnArchiveIcon from '@material-ui/icons/Unarchive';
import WatchLaterIcon from '@material-ui/icons/WatchLater';
import WatchLaterOutlinedIcon from '@material-ui/icons/WatchLaterOutlined';
// Custom
import TextFieldCounter from '../../common/TextFieldCounter';
import bookmarkActions from '../../../_actions/bookmark.actions';
import Tag from '../../common/Tag';
import TagsInputField from '../../common/TagsInputField';
import FormValidation from '../../common/FormValidation';
import withValidation from '../../hoc/withValidation';

const ValidatedTextField = withValidation(TextField);
const ValidatedTextFieldCounter = withValidation(TextFieldCounter);

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const styles = theme => ({
  cancel: {
    color: theme.palette.text.secondary
  },
  tagInput: {
    marginBottom: theme.spacing.unit * 4
  },
  flags: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  flag: {
    zoom: 1.2,
    marginLeft: 0
  }
});

class EditBookmarkForm extends React.Component {
  state = {
    title: this.props.item.title,
    description: this.props.item.description,
    url: this.props.item.url,
    flags: this.props.item.flags,
    tags: this.props.item.tags
  };

  /**
   * Because of TagsInputField, we can't simply retrieve fields value via event.target.value in the handleSuubmit
   * metod. Therefore, we have to go with internal state updated on
   */
  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.props.editOperationState.loading && !nextProps.editOperationState.loading) {
      const newValues = {
        title: nextProps.item.title,
        description: nextProps.item.description,
        url: nextProps.item.url,
        flags: nextProps.item.flags,
        tags: this.props.item.tags
      };
      this.setState(newValues);
    }
  }

  handleSubmit = event => {
    event.preventDefault();
    this.props.editItem(this.props.item.bid, this.state);
  };

  handleInputChange = e => {
    const { name, value } = e.target;
    const newState = this.state;
    newState[name] = value;
    this.setState(newState);
  };

  handleTagsChange = tags => {
    const newValues = this.state;
    newValues.tags = tags;
    this.setState(newValues);
  };

  handleCheckboxChange = e => {
    const { checked, value } = e.target;
    const newValues = this.state;
    newValues.flags = { ...newValues.flags, [value]: checked };
    this.setState(newValues);
  };

  render() {
    const { classes, fullScreen, editOperationState } = this.props;

    return (
      <Dialog
        fullWidth
        fullScreen={fullScreen}
        open={this.props.open}
        TransitionComponent={Transition}
        onClose={this.props.onClose}
        aria-labelledby="edit-form-title"
      >
        {editOperationState.loading && <LinearProgress />}
        <DialogTitle id="edit-form-title">{'Edit bookmark'}</DialogTitle>
        <DialogContent>
          <FormValidation onSubmit={this.handleSubmit} validation={['title', 'url']}>
            <ValidatedTextFieldCounter
              fullWidth
              required
              label="Title"
              id="title"
              name="title"
              margin="dense"
              value={this.state.title}
              characterLimit={255}
              onChange={this.handleInputChange}
              helperText={'A meaningful title to help you find your bookmark.'}
            />
            <ValidatedTextField
              required
              fullWidth
              type="url"
              pattern="^https?\:\/\/"
              placeholder="https://..."
              autoFocus={true}
              value={this.state.url}
              label="Bookmark URL"
              id="url"
              name="url"
              margin="dense"
            />
            <ValidatedTextFieldCounter
              fullWidth
              multiline={true}
              label="Description"
              id="description"
              name="description"
              margin="dense"
              value={this.state.description}
              onChange={this.handleInputChange}
              characterLimit={300}
              helperText={
                'A meaningful description text to help you remember the content of this bookmark.'
              }
            />

            <TagsInputField
              fullWidth
              label="Tags"
              id="tags"
              name="tags"
              margin="dense"
              maxNumberOfTags={20}
              maxTagSize={25}
              newChipKeyCodes={[32, 13]}
              className={classes.tagInput}
              defaultValue={this.state.tags}
              onChange={this.handleTagsChange}
              alterValueOnChange={value => value.replace(/\s/g, '_').toLowerCase()}
              chipRenderer={(
                { prefix, text, isFocused, isDisabled, handleClick, handleDelete, className },
                key
              ) => (
                <Tag
                  key={key}
                  className={className}
                  style={{
                    pointerEvents: isDisabled ? 'none' : undefined,
                    backgroundColor: isFocused ? blue[300] : undefined
                  }}
                  onClick={handleClick}
                  onDelete={handleDelete}
                  label={`${prefix}${text}`}
                />
              )}
              helperText={'Add tags to help you categorize your content.'}
            />
            <div className={classes.flags}>
              <FormControlLabel
                id="favorite"
                name="favorite"
                className={classes.flag}
                control={
                  <Checkbox
                    value="favorite"
                    color="default"
                    icon={<StarBorderIcon fontSize="small" />}
                    checkedIcon={<StarIcon fontSize="small" />}
                    checked={this.state.flags.favorite}
                    onChange={this.handleCheckboxChange}
                  />
                }
                label="Favorite"
              />
              <FormControlLabel
                id="read_later"
                name="read_later"
                className={classes.flag}
                control={
                  <Checkbox
                    value="read_later"
                    color="default"
                    icon={<WatchLaterOutlinedIcon fontSize="small" />}
                    checkedIcon={<WatchLaterIcon fontSize="small" />}
                    checked={this.state.flags.read_later}
                    onChange={this.handleCheckboxChange}
                  />
                }
                label="Read Later"
              />
              <FormControlLabel
                id="archive"
                name="archive"
                className={classes.flag}
                control={
                  <Checkbox
                    value="archive"
                    color="default"
                    icon={<ArchiveOutlinedIcon fontSize="small" />}
                    checkedIcon={<UnArchiveIcon fontSize="small" />}
                    checked={this.state.flags.archive}
                    onChange={this.handleCheckboxChange}
                  />
                }
                label="Archive"
              />
            </div>

            <DialogActions>
              <Button
                autoFocus={true}
                type="submit"
                disabled={editOperationState.loading}
                color="primary"
              >
                Save changes
              </Button>
              <Button
                disabled={editOperationState.loading}
                onClick={this.props.onClose}
                className={classes.cancel}
              >
                Cancel
              </Button>
            </DialogActions>
          </FormValidation>
        </DialogContent>
      </Dialog>
    );
  }
}

// Inject dispatch.
function mapDispatchToProps(dispatch) {
  return {
    editItem: (bid, data) => dispatch(bookmarkActions.update(bid, data))
  };
}

function mapStateToProps(state) {
  const editOperationState = state.bookmarks.operations.UPDATE;
  return {
    editOperationState
  };
}

const ConnectedEditBookmarkForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditBookmarkForm);

const StyledConnectedEditBookmarkForm = withStyles(styles)(ConnectedEditBookmarkForm);
export default withMobileDialog()(StyledConnectedEditBookmarkForm);
