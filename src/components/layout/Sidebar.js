import React from 'react';
import classNames from 'classnames';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import { withStyles } from '@material-ui/core/styles';

import {
  BookmarkPredefinedFilterMenu,
  BookmarkCustomFilterMenu
} from '../bookmarks/menu/BookmarkFilterMenus';

const drawerWidth = 240;

const styles = theme => ({
  drawerPaper: {
    position: 'absolute',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing.unit * 7
  },
  toolbarIcon: {
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: `0 ${theme.spacing.unit}px`,
    ...theme.mixins.toolbar
  }
});

class Sidebar extends React.Component {
  state = {
    open: false
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;

    return (
      <Drawer
        onMouseEnter={this.handleDrawerOpen}
        onMouseLeave={this.handleDrawerClose}
        variant="permanent"
        classes={{
          paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose)
        }}
        open={this.props.open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={this.handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <BookmarkPredefinedFilterMenu open={this.state.open} />
        </List>
        <Divider />
        <List>
          <BookmarkCustomFilterMenu open={this.state.open} />
        </List>
      </Drawer>
    );
  }
}

export default withStyles(styles)(Sidebar);
