import React from 'react';
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
// Icons
import AddIcon from '@material-ui/icons/Add';
// Custom
import CreateBookmarkForm from '../form/CreateBookmarkForm';

const styles = () => ({
  button: {
    width: 36,
    height: 32,
    marginTop: -2,
    marginRight: 8
  }
});

class CreateIconButton extends React.Component {
  state = {
    createDialog: false
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.createOperationState.loading === true &&
      !this.props.createOperationState.loading
    ) {
      this.setState({ createDialog: false });
    }
  }

  handleCreateDialogOpen = () => {
    this.setState({ createDialog: true });
  };

  handleCreateDialogClose = () => {
    this.setState({ createDialog: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Fab
          title="Add new bookmark"
          aria-label="Add new bookmark"
          color="primary"
          className={classes.button}
          size="small"
          aria-haspopup="true"
          onClick={this.handleCreateDialogOpen}
        >
          <AddIcon />
        </Fab>
        <CreateBookmarkForm open={this.state.createDialog} onClose={this.handleCreateDialogClose} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const createOperationState = state.bookmarks.operations.CREATE;
  return {
    createOperationState
  };
}

const ConnectedCreateIconButton = connect(mapStateToProps)(CreateIconButton);

export default withStyles(styles)(ConnectedCreateIconButton);
