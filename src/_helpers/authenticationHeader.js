import storageService from '../_services/storage.service';

function authHeader() {
  // return authorization header with jwt token
  const user = storageService.get('user');

  if (user && user.token) {
    return { 'X-Auth-Token': user.token };
  }
  return { 'X-Auth-Token': 'unknown' };
}

export default authHeader;
